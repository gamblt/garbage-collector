<?php
/**
 * Mail Templates enum
 *
 * @package MailTemplates
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Enums;

/***/
class MailTemplates extends \Garbage\Enums\Base\Enum
{
    const HELLO             	= 1001;
    const SHELTER_APPROVED  	= 1011;
    const SHELTER_REJECTED  	= 1012;
    const ORGANIZATION_CONTACT  = 1020;
    const SHELTER_HELP      	= 1021;
    const USER_CONTACT 			= 1030;
    /**
     * @copydoc
     */
    public static function getStrings() {}
}