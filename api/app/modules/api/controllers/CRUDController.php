<?php
/**
 * Base API CRUD Controller
 *
 * @package APICRUDController
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\API\Controllers;

use Phalcon\Mvc\Controller;

use Garbage\Enums\ResponseStatus;

/***/
class CRUDController extends BaseController
{

    /**
     * Current Model name
     * @var string
     */
    protected $_model_name;

    /**
     * Access restrinctions array
     * @var array
     */
    protected $_edit_restrictions = [];

    /**
     * Return Select params for select list
     *
     *  @return array
     */
    protected function list_params()
    {
        return [];
    }


    /**
     * Return Select params for info request
     *
     * @return array
     */
    protected function info_params()
    {
        return [];
    }

   /**
     * Prepare output data
     * @param string $mode Select mode 'list'/'info'
     * @param object &$object Item object
     * @param array &$array Output item array
     */
    protected function post_select($mode, &$object, &$array)
    {

    }

    /**
     * Pre Insert routines (fill fields for example)
     *
     * @param object &$object
     */
    protected function pre_insert(&$object, $mode = 'undefined')
    {

    }

    /**
     * Post Insert routines (clear cache for example)
     *
     * @param object &$object
     */
    protected function post_insert(&$object)
    {

    }

    /**
     * Pre Edit routines (save some old data, etc)
     *
     * @param string $field Fields name
     * @param mixed $value Field value
     * @param object &$object Model object
     */
    protected function pre_edit($field, &$value, &$object)
    {

    }

    /**
     * Post Edit routines (send events for example)
     * @param string $field Fields name
     * @param mixed $value Field value
     * @param object &$object Model object
     */
    protected function post_edit($field, $value, &$object)
    {

    }

    /**
     * Pre Delete routines (Check permissions)
     *
     * @param object &$object Model object
     * @param string &$error Error
     *
     * @return bool (false to prevent deletion)
     */
    protected function pre_delete($object, &$error = '')
    {
        return true;
    }

    /**
     * @apiDescription Prepare output data
     *
     * @apiDefine APIListSuccess
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Array} list Items List
     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "list" : [],
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    public function listAction()
    {
        $params = $this->list_params();
        $this->logger->log(LOG_INFO,"Model [".$this->_model_name."] params [".print_r($params, true)."]");
        $list_obj = call_user_func(
            'Garbage\Shared\Models\\'.$this->_model_name.'::find',
            $params
        );
        $list = $list_obj->toArray();
        //Prepare output data
        $this->post_select('list',$list_obj, $list);

        //Total items calculate...
        $total = count($list);
        $this->logger->log(LOG_INFO,"ItemsListCount [".$total."]");
        //Calculate total items for pagination if offset exists
        if (isset($params['offset'])) {
            $params['offset'] = 0;
            if (isset($params['limit'])) {
                $params['limit'] = 'ALL';
            }
            unset($params['order']);
            $total = call_user_func(
                'Garbage\Shared\Models\\'.$this->_model_name.'::count',
                $params
            );
            $this->logger->log(LOG_INFO,"ItemsTotalCount [".$total."]");
        }


        $this->_response['status']  = ResponseStatus::SUCCESS;
        $this->_response['message'] = 'success';
        $this->_response['list']    = $list;
        $this->_response['total']   = $total;
        return $this->send();
    }

    /**
     * @apiDescription Get item info
     *
     * @apiDefine APIInfoSuccess
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Array} info Item Info
     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : [],
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    public function infoAction()
    {
        $params = $this->info_params();
        $id = $this->request->getPost("id", "int", 0);
        //If no params array - use element id
        if (empty($params)) {
            $params = (int)$id;
        }
        //$info_obj = $this->_model_name::findFirst($id);
        $info_obj = call_user_func(
            'Garbage\Shared\Models\\'.$this->_model_name.'::findFirst',
            $params
        );

        if(!$info_obj) {
            $this->_response['message']  = 'Element of '.$this->_model_name.' not found';
            return $this->send();
        }
        $info = $info_obj->toArray();
        //Prepare output data
        $this->post_select('info',$info_obj, $info);
        $this->_response['status']  = ResponseStatus::SUCCESS;
        $this->_response['message']  = 'success';
        $this->_response['info']  = $info;
        return $this->send();
    }

    /**
     * @apiDescription Add to DataBase a new item and fill all editable model attributes from request.
     *
     * @apiDefine APIAddSuccess
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {json} params Added object
     * @apiSuccess {String} message Status
     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "created",
    "params": {},
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    /**
     * @apiDefine APIAddError
     *
     * @apiError {Integer} status Status Code.
     * @apiError {String} message Status
     * @apiError {Array} errors Errors list
     * @apiError {String} [tkey] CSRF token name
     * @apiError {String} [tval] CSRF token value
     *
     * @apiErrorExample {json} Error-Response:
{
    "status" : 500,
    "message" : "error",
    "errors": ["Item save error"],
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    public function addAction()
    {
        if ($this->request->isPost()
            //&& $this->request->isAjax()
            && $this->security->checkToken()
        ) {
            $id = $this->request->getPost("id",  "int", 0);
            $class_name = 'Garbage\Shared\Models\\'.$this->_model_name;

            $mode = 'edit';
            $item = call_user_func(
                'Garbage\Shared\Models\\'.$this->_model_name.'::findFirst',
                $id
            );
            if(!$item) {
              $mode = 'create';
              $item = new $class_name();
              $item->user_id  = $this->session->id;
              $item->user_id = $this->session->id;
            } else {
                if ($item->user_id != $this->session->id) {
                    $this->_response['message']  = "Error";
                    $this->_response['info']  = "Only owner can edit a record!";
                    return $this->send();
                }
            }

            $metaData   = $item->getModelsMetaData();
            $attributes = $metaData->getAttributes($item);
            $editable_fields = $item->getEditableFields();
            foreach ($attributes as $attr) {
                if (in_array($attr, $editable_fields))
                {
                    $errors = [];
                    //If field exists in post array and editable - assign it to model
                    $value = $this->request->getPostSanitized($attr, false);
                    if ($value !== false) {
                        $this->pre_edit($attr, $value, $item);
                        if ($item->editField($attr, $value, [], $errors, true)) {
                            $this->post_edit($attr, $value, $item);
                        }
                    }
                }
            }
            $this->pre_insert($item, $mode);
            if ($item->save()) {
                $this->post_insert($item);
                $this->_response['status']  = ResponseStatus::SUCCESS;
                $this->_response['message']  = "Created";
                $this->_response['info']  = $item;
            } else {
                $errors = [];
                foreach ($item->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }

                $this->_response['message']  = "Item save error";
                $this->_response['errors']  = $errors;
            }
        } else {
            if (!$this->request->isPost())
              $this->_response['message']  = "Invalid post";
            elseif (!$this->request->isAjax())
              $this->_response['message']  = "Invalid ajax";
            elseif (!$this->security->checkToken())
              $this->_response['message']  = "Invalid token";
        }
        return $this->send();
    }


    /**
     * Edit item fields
     *
     * @apiDefine APIEditSuccess
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "saved",
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    /**
     * @apiDefine APIEditError
     *
     * @apiError {Integer} status Status Code.
     * @apiError {String} message Status
     * @apiError {Array} errors Errors list
     * @apiError {String} [tkey] CSRF token name
     * @apiError {String} [tval] CSRF token value
     *
     * @apiErrorExample {json} Error-Response:
{
    "status" : 500,
    "message" : "error",
    "errors": ["Edit model field error"],
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    public function editAction()
    {
        $id = $this->request->getPost("id", "int", 0);
        $this->logger->log(LOG_INFO,'Edit Model field ID['.$id.']');

        $is_post = $this->request->isPost();
        $is_checked = $this->security->checkToken();

        if ($id && $is_post && $is_checked)
        {
            // Get item
            //$this->logger->log(LOG_INFO,'Select particular model...');
            $item = call_user_func(
                'Garbage\Shared\Models\\'.$this->_model_name.'::findFirst',
                $id
            );
            if(!$item) {
                $this->_response['message']  = 'Element of '.$this->_model_name.' not found';
                return $this->send();
            }

            if (!($item->checkEditAccess($this->session))) {
                $this->_response['message']  = "Error";
                $this->_response['info']  = "Edit denied!";
                return $this->send();
            }

            $errors = [];
            $metaData   = $item->getModelsMetaData();
            $attributes = $metaData->getAttributes($item);
            $editable_fields = $item->getEditableFields();
            foreach ($attributes as $attr) {
                if (in_array($attr, $editable_fields))
                {
                    $errors = [];
                    if (isset($_POST[$attr]))
                    {
                        //If field exists in post array and editable - assign it to model
                        $value = $this->request->getPostSanitized($attr, false);
                        //Run pre_edit function
                        $this->pre_edit($attr, $value, $item);
                        if ($item->editField(
                            $attr, $value,
                            $this->_edit_restrictions, $errors, true)
                        ) {
                            $this->logger->log(LOG_INFO,'Edit Model['.$this->_model_name.'] Field['.$attr.'] Value['.$value.']');

                            $this->post_edit($attr, $value, $item);
                        }
                    }
                }
            }
            $this->logger->log(LOG_INFO,"ITEM[".print_r($item->toArray(), true)."]");
            if (!($item->save())) {
                foreach ($item->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
            }

            if (empty($errors)) {
                $this->_response['status']  = ResponseStatus::SUCCESS;
                $this->_response['message']  = 'saved';
            } else {
                $this->logger->log(LOG_ERR,'Edit model failed...['.print_r($errors, true).']');
                $this->_response['errors'] = $errors;
            }
        } else {
            $this->logger->log(LOG_INFO,'Invalid ID/POST/Token');
        }
        return $this->send();
    }

    /**
     * Delete item
     *
     * @apiDefine APIDeleteSuccess
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "deleted",
    "tkey":'xxx',
    "tval":'yyy'
}
     */

    /**
     * @apiDefine APIDeleteError
     *
     * @apiError {Integer} status Status Code.
     * @apiError {String} message Error
     * @apiError {String} [tkey] CSRF token name
     * @apiError {String} [tval] CSRF token value
     *
     * @apiErrorExample {json} Error-Response:
{
    "status" : 500,
    "message" : "error",
    "errors": ["Error:..."],
    "tkey":'xxx',
    "tval":'yyy'
}
     */
    public function deleteAction()
    {
        $id     = $this->request->getPost("id",  "int", 0);

        if ($id
            && $this->request->isPost()
            && $this->security->checkToken()
        ) {
            // Get item
            $item = call_user_func(
                'Garbage\Shared\Models\\'.$this->_model_name.'::findFirst',
                $id
            );
            if($item) {

                if ($item->user_id != $this->session->id) {
                    $this->_response['message']  = "Error";
                    $this->_response['info']  = "Only owner can delete a record!";
                    return $this->send();
                }
                if ($this->pre_delete($item)) {
                    if ($item->delete())
                    {
                        $this->_response['status']  = ResponseStatus::SUCCESS;
                        $this->_response['message']  = 'deleted';
                    }
                }
            } else {
                    $this->_response['message']  = 'no '.$this->_model_name.' item';
            }
        }
        return $this->send();
    }
}
