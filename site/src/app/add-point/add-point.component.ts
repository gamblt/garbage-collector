import { Component, OnInit } from '@angular/core';
import { LocationService } from '../services';

declare var _:any;

@Component({
  selector: 'app-add-point',
  templateUrl: './add-point.component.html',
  styleUrls: ['./add-point.component.scss']
})
export class AddPointComponent implements OnInit
{
  lat: number;
  lng: number;

  constructor(
    private _locationService: LocationService
  ) { }

  ngOnInit() {
    let location: google.maps.LatLng = this._locationService.getLocation();
    console.log('Add point location [%o]', location);
    if (location) {
      this.lat = _.round(location.lat(), 7);
      this.lng = _.round(location.lng(), 7);
    } else {

    }
  }

}
