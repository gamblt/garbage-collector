export interface IPoint {
    id?:          number;
    lat:          number;
    lng:          number;
    type?:        number;
    title?:       string;
    description?: string;
}
