<?php
/**
 * Common Utils component
 *
 * @package Utils
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

class Utils extends Component
{

    /**
     * Set cookie for sign-up popup
     *
     * @return type
     */
    public static function setSignupCookie() {
        setcookie( "signupview", 'hide', strtotime( '+30 years' ), '/');
    }


    /**
     * Return random string
     * @example $token = Utils::generateRandom(64);
     *
     * @param int $length
     *
     * @return string
     */
    public static function generateRandom($length = 32)
    {
        $fp = @fopen('/dev/urandom','rb');
        if ($fp !== FALSE) {
            $data = @fread($fp, $length);
            @fclose($fp);
        } else {
            while( @$c++ * 16 < $length )
                @$tmp .= md5( mt_rand(), true );
            $data = substr( $tmp, 0, $length );
       }

       $result = base64_encode($data);
        // remove none url chars
        $result = preg_replace('/[^A-Za-z0-9]/', '0', $result);
        $result = substr($result, 0, $length);
        return $result;
    }

    /**
     * Localize date
     * @deprecated
     *
     * @param \DateTime $date
     *
     * @return string
     */
    /*public static function localizeDate($date)
    {
        $date_str = $date->format('d')." ";

        switch ($date->format('n')){
        case 1: $date_str  .= 'января'; break;
        case 2: $date_str  .= 'февраля'; break;
        case 3: $date_str  .= 'марта'; break;
        case 4: $date_str  .= 'апреля'; break;
        case 5: $date_str  .= 'мая'; break;
        case 6: $date_str  .= 'июня'; break;
        case 7: $date_str  .= 'июля'; break;
        case 8: $date_str  .= 'августа'; break;
        case 9: $date_str  .= 'сентября'; break;
        case 10: $date_str .= 'октября'; break;
        case 11: $date_str .= 'ноября'; break;
        case 12: $date_str .= 'декабря'; break;
        }
        return $date_str." ".$date->format('Y');
    }*/


    /**
     * Return unix time with microseconds
     *
     * @return int
     */
    public static function microtimeCnt()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((int)$sec*1000000 + (int)($usec*1000000));
    }


    /**
     * Transliteration russion words
     *
     * @param string $str Original string
     *
     * @return string Return string
     */
    public static function translit($str)
    {
        $rus = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
        $lat = ['A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya'];
        return str_replace($rus, $lat, $str);
    }

    /**
     * Create alias from russian/english names
     *
     * @param string $str Origin string
     *
     * @return string Alias
     */
    public static function createAlias($str)
    {
        //Trim string;
        $str = trim(mb_strtolower($str));
        //Remove invalid chars
        $str = preg_replace('/[^\w -]/u', '', $str);
        return str_replace(' ', '-', static::translit($str));
    }

    /**
     * Replace rows and columns of array
     *
     * @param array &$array Input array
     *
     * @return array
     */
    public static function array_transposition($array)
    {
        array_unshift($array, null);
        $array = call_user_func_array("array_map", $array);
        return $array;
    }

    /**
     * String to boolean converter for Ajax request
     * @param type $value
     * @return type
     */
    public static function str2bool($value)
    {
        // Stringify src value
        //ob_start(); var_dump($value); $src = ob_get_contents(); ob_end_clean();

        if (($value !== true) && ($value !== false)) {
            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
            //$value = (in_array($value, ['true','on','1',1])) ? true : false;
        }

        // Stringify dst value
        //ob_start(); var_dump($value); $dst = ob_get_contents(); ob_end_clean();
        //Log stringify values
        //$di = \Phalcon\DI::getDefault(); $logger = $di->getShared('logger');
        //$logger->log(LOG_INFO,'BoolConverter ['.$src.']=>['.$dst.']');
        
        return $value;
    }


    /**
     * Convert timestamp to DB time format (Y-m-d H:i:s+00)
     *
     * @param int $timestamp Timestamp
     * @param array &$errors Convert errors
     *
     * @return string|false if convertation failed
     */
    public static function ts2time($timestamp, &$errors = [])
    {
        $ts = (int)$timestamp;
        //If datetime is timestamp
        if ($ts == $timestamp) {
            $dt =  new \DateTime();
            $dt->setTimezone(new \DateTimeZone('UTC'));
            $dt->setTimestamp($timestamp);
        } else {
            return false;
        }
        if ($dt instanceof \DateTime) {
            return $dt->format('Y-m-d H:i:s+00');
        } else {
            $errors[] = "Invalid value";
            return false;
        }
    }

    /**
     * Create tags for links/images/youtube videos if its not exists
     *
     * @param string $str Original text
     *
     * @return string
     */
    public function linkWrapper($str)
    {
        $extensions = [
            'jpg',
            'jpeg',
            'png',
            'gif'
        ];

        $this->logger->log(LOG_INFO,"UTILS::WRAPPER:: Original string [".$str."]");
        $str = preg_replace_callback(
        '#(?:"?https?://\S+)|(?:www.\S+)|(?:'.implode('|', $extensions).'\"?)#',
        function($arr) use ($extensions)
        {
            //echo "\n\n";
            $url = $arr[0];

            //echo "SOURCE[".$url."]\n\n";
            if (in_array(strtolower($url), $extensions)) {
                echo "In array!";
                $this->logger->log(LOG_INFO,"UTILS::WRAPPER:: Extension found. Ignore...");
                //Nothing changed!
                return $arr[0];
            //} else {
                //echo "Not in array [".strtolower($url)."]\n";
            }
            //Remove last open tag if exists
            $pos = strpos($url, '<');
            $suffix = '';
            if(($pos !== false) && ($pos > 0)) {
                //echo "Tag found[".$url."]\n";
                $apos = strpos($url, '</a>');
                if (!$apos) {
                    $suffix = substr($url, $pos);
                    $url = substr($url, 0, $pos);
                } else {
                    $this->logger->log(LOG_INFO,"UTILS::WRAPPER:: <a> tag position found! Ignore...");
                    //Nothing changed!
                    return $arr[0];
                }
            }
            //echo "Suffix [".$suffix."]\n";
            $orig = $url;

            $url = trim($url, '"');
            if ($orig != $url) {
                //echo "Tagged url!\n";
                //Nothing changed!
                return $arr[0];
            } else {
                //echo "Native URL\n";
                //echo "URL0[".$url."]\n";
                $pos = strpos($url, '"');
                if(($pos !== false) && ($pos > 0)) {
                    $url = substr($url, 0, $pos);
                }
               //echo "URL_[".$url."]\n\n";

                //Normalize url (Add http)
                if((strpos($url, 'http://') === false)
                && (strpos($url, 'https://') === false)
                ) {
                    $url = 'http://' . $url;
                }
                $parsed_url = parse_url($url);
                //echo "[".print_r($parsed_url, true)."]\n\n";

                // Images
                if(preg_match('#\.(png|jpg|gif)$#', $parsed_url['path'])) {
                    return '<img src="'. $url . '" alt="image"/>'.$suffix;
                }

                // Youtube
                if(in_array($parsed_url['host'], array('www.youtube.com', 'youtube.com'))
                  && $parsed_url['path'] == '/watch'
                  && isset($parsed_url['query']))
                {
                    parse_str($parsed_url['query'], $query);
                    $data = sprintf('<iframe class="embedded-video" src="http://www.youtube.com/embed/%s" allowfullscreen></iframe>', $query['v']);
                    return $data.$suffix;
                }

                //Links
                $data = sprintf('<a href="%1$s">%1$s</a>', $url);
                return $data . $suffix;
            }
        }, $str);
        return $str;
    }

    public static function utf8_json_encode($data)
    {
        foreach ($data as $key => $value) {
            $data[$key] = mb_convert_encoding($value,'UTF-8','UTF-8');
        }
        return json_encode($data);
    }


    /**
     * Cut double space and bank-cards
     * @param string $text Original text
     * @return string
     */
    public static function normalizeText($text)
    {
        //Remove multi-space
        $text = preg_replace('/\s+/', ' ', $text);
        //Remove credit card numbers
        $text = preg_replace('/((4\d{3})|(5[1-5]\d{2}))(-?| ?)(\d{4}(-?| ?)){3}|^(3[4,7]\d{2})(-?| ?)\d{6}(-?| ?)\d{5}/', '', $text);
        return $text;
    }

    public function age2string($birthday_ts) 
    {
        $bday = new \DateTime();
        $bday->setTimestamp($birthday_ts);
        $now = new \DateTime();
        $interval = $now->diff($bday);

        $bday_str = '';
        $y = $interval->y;
        if ($y > 0) {
          $bday_str .= $y.' '.$this->translate->_nt("YEARS", $y).' ';
        }
        $m = $interval->m;
        if ($m > 0) {
          $bday_str .= $m.' '.$this->translate->_nt("MONTHS", $m).' ';
        }
        if (($m + $y) == 0) {
          $bday_str .= 'Менее месяца';
        }
        return trim($bday_str);
    }
}