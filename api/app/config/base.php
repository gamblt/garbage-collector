<?php

return [
    'debug' => false,
    'version' => '0.0.0',
    'build' => include __DIR__.'/build/build.php',
    'database' => [
        'host'      => 'localhost',
        'port'      => 5432,
        'username'  => 'user',
        'password'  => 'password',
        'dbname'    => 'garbage'
    ],
    // Session config
    'session' => [
        'sess'=> 'trash',    // Session cookies name
        'ttl' => 2592000,   // 30 days session livetime;
        'secure' => false,
        'redis' => [
          'host'      => '127.0.0.1',
          'port'      => 6379,
          'timeout'   => 5,
          'password'  => 'pass',
          'prefix'    => 'garbage.local',
          'db'        => 1
        ]
    ],
    /* Redis config for tokens saving */
    'tokenmanager' => [
      'host'      => '127.0.0.1',
      'port'      => 6379,
      'timeout'   => 5,
      'password'  => 'pass',
      'prefix'    => 'garbage.local',
      'db'        => 2
    ],
    // Security tokens generator config
    'security' => [
      'hash_secret_key' => 'krugnwylriuywmefvgyeeaunvdmhwrekwygfwcef'
    ],
    'redis' => [
        'host'      => '127.0.0.1',
        'port'      => 6379,
        'timeout'   => 5,
        'password'  => 'pass',
        'prefix'    => 'garbage.local',
        'db'        => 3
    ],
    // Main Cache config
    'cache' => [
        'lifetime' => 86400
    ],
    'image' => [
      'temp_path'   => __DIR__ . '/../tmp/',
      'upload_path' => __DIR__ . '/../../../site/images/',
      'upload_url'  => '/images/'
    ],
    'avatar' => [
      'temp_path'   => __DIR__ . '/../tmp/',
      'upload_path' => __DIR__ . '/../../../site/avatars/',
      'upload_url'  => '/avatars/'
    ],
    'params' => [
      'ask_to_mail' => 'admin@mg.od.ua.org'
    ]
];
