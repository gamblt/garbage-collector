<?php
/**
 * @package Logger;
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;
//use Phalcon\Logger\Adapter\File as SyslogAdapter;

/***/
class Logger extends Component
{
    /**
     * Logger constructor
     * @return type
     */
    function __construct($city = "default") {
        if (empty($city)) {
            $city = "default";
        }
        $selector = 'GARBAGE-'.strtoupper($city);
        //echo "Selector[".$selector."]"; exit();
       openlog($selector, LOG_PID | LOG_PERROR, LOG_LOCAL0);
    }

    /**
     * Write log
     * @param int $priority - LOG_INFO, LOG_ERR
     * @param string $message - error message
     */
    public function log($priority, $message) {

            syslog($priority, $message);
            if (defined("ECHO_LOGS")) {
                echo print_r($message, true)."\n";
            }
    }

    /**
     * Log model errors
     * @param int $priority
     * @param object $model
     */
    public function logModel($priority, $model) {
        foreach ($model->getMessages() as $message) {
            syslog($priority,
                "Message: ". $message->getMessage().
                "] Field:". $message->getField().
                "] Type:[". $message->getType().
                "]");
        }
    }
}
