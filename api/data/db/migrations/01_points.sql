-- Table: public.points

DROP TABLE IF EXISTS public.points;

CREATE TABLE public.points
(
  id bigserial,
  user_id integer,
  device_id integer,
  lat double precision,
  lng double precision,
  type integer,
  title character varying,
  description character varying,
  image character varying,
  position geometry,
  approved boolean DEFAULT true,
  create_datetime timestamp with time zone DEFAULT now(),
  CONSTRAINT points_id_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.points
  OWNER TO garbage;


--UPDATE points SET position = ST_SetSRID(ST_MakePoint(lng, lat), 4326);
--SELECT ST_AsText(position), lat, lng, ST_Distance(ST_GeomFromText('POINT(46.477684 30.746452)', 4326), position) from points;