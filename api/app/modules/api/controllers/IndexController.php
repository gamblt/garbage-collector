<?php
/**
 * Index API Controller
 *
 * @package APIIndexController
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\API\Controllers;

use Phalcon\Mvc\Controller;
use Garbage\Enums\ResponseStatus;

/***/
class IndexController extends BaseController
{
    /**
     * Test API Action
     */
    public function indexAction()
    {
        echo "<pre>";
        echo "Test\n";

        define('ECHO_LOGS', 1);

        echo "End\n";
        exit();
        
        //$this->_response['message']  = "success";
        //return $this->send();
    }

    public function notfoundAction()
    {
        return $this->send();
    }
}
