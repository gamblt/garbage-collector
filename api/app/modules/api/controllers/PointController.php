<?php
/**
 * Profile API Controller
 *
 * @package APIProfileController
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\API\Controllers;

use Phalcon\Mvc\Controller;

use Garbage\Components\Image;
use Garbage\Components\Utils;

use Garbage\Shared\Models\Points;

use Garbage\Enums\Species;
use Garbage\Enums\PointType;
use Garbage\Enums\ResponseStatus;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

/***/
class PointController extends CRUDController
{
    protected $_model_name = 'Points';
    /**
     * @apiDescription Add new point
     *
     * @api {post} /api/point/add Add
     * @apiName PointAdd
     * @apiGroup API-Point
     * @apiVersion 0.0.1
     *
     * @apiParam {Float} lat Point latitude
     * @apiParam {Float} lng Point longitude
     * @apiParam {Integer=0,1,2,3,4} [type="0"] Point type
     * @apiParam {String} [name] Point name
     * @apiParam {String} [description] Point description
     * @apiParam {File} [image] Point image
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Object} point Point Info - (DEPRECATED!!! Use info field)

     * @apiSuccess {Object} info Point Info
     * @apiSuccess {Number} info.id   Id.
     * @apiSuccess {Float}  info.lat  Latitude
     * @apiSuccess {Float}  info.lng  Longitude
     * @apiSuccess {Number} info.type Type.
     * @apiSuccess {String} info.name Name.
     * @apiSuccess {String} info.description Description.
     * @apiSuccess {String} info.image Image.
     * @apiSuccess {Integer} info.create_datetime Create datetime timestamp.

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : {},
}
     *
     * @apiUse APIAddError
     */
    public function addAction()
    {
        $this->logger->log(LOG_INFO,"POINT:: Server. [".print_r($_SERVER, true)."]");
        $this->logger->log(LOG_INFO,"POINT:: GET:[".print_r($_GET, true)."]");
        $this->logger->log(LOG_INFO,"POINT:: POST:[".print_r($_POST, true)."]");

        $lat    = floatval($this->request->getPost("lat", "float", "0.0"));
        $lng    = floatval($this->request->getPost("lng", "float", "0.0"));
        $type   = $this->request->getPost("type", "int", 0);
        $title  = $this->request->getPost("title", "striptags", "");
        $description   = $this->request->getPost("description", "striptags", "");
        $species = $this->request->getPost("species", "striptags", "[]");

        $this->logger->log(LOG_INFO,"POINT:: Add Point Info
            Lat:[".$lat."]
            Long:[". $lng."]
            Type:[". $type."]
            Name:[". $title."]
            Desc:[". $description."]"
        );

        if(empty($lat) || empty($lng))
        {
            $this->logger->log(LOG_INFO,"POINT:: Invalid request. Empty required fields!!!");
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'Invalid request. Empty required fields';
            return $this->send();
        }

        $avaliable_types = PointType::getStrings();
        if(!isset($avaliable_types[$type]))
        {
            $this->logger->log(LOG_INFO,"POINT:: Invalid type! [".$type."]");
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'Invalid type. Types avaliable ['.implode(', ', array_keys($avaliable_types)) .']';
            return $this->send();
        }

        $this->logger->log(LOG_INFO,"POINT:: Add Point...");
        $point = new Points();
        $point->lat = $lat;
        $point->lng = $lng;
        $point->type = $type;
        $point->title = $title;
        $point->device_id = $this->device->id;
        $point->description = $description;
        $point->approved = true;
        $point->species = $species;

        $image = $this->uploadImage();
        if (!empty($image)) {
            $point->image = $image;
        }

        $this->logger->log(LOG_INFO,"POINT:: Save point...");
        if ($point->save())
        {
            $this->logger->log(LOG_INFO,"POINT:: Point saved!");
            $point->refresh();
            $info = $point->toArray();

            if (isset($info['species'])) {
                $info['species'] = json_decode($info['species'], true);
            }
            $info['create_datetime'] = $point->create_datetime_ts;
            unset($info['approved']);

            $this->_response['status']  = ResponseStatus::SUCCESS;
            $this->_response['message'] = 'success';
            $this->_response['info'] = $info;
            $this->_response['point'] = $info;
        } else {
            $this->logger->log(LOG_INFO,"POINT:: Point save error!");
            $this->logger->logModel(LOG_ERR, $point);
            $this->_response['message'] = 'error';
            $this->_response['errors'][]   = 'Point save error';
            unlink($opath);
        }
        return $this->send();
    }


    /**
     * @apiDescription Edit exists point if device is owner of this point
     *
     * @api {post} /api/point/edit Edit
     * @apiName PointEdit
     * @apiGroup API-Point
     * @apiVersion 0.0.1
     *
     * @apiParam {Integer} id Point ID
     * @apiParam {Float} [lat] Point latitude
     * @apiParam {Float} [lng] Point longitude
     * @apiParam {Integer=0,1,2,3,4,5} [type] Point type
     * @apiParam {String} [name] Point name
     * @apiParam {String} [description] Point description
     * @apiParam {File} [image] Point image
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Object} info Point Info
     * @apiSuccess {Number} info.id   Id.
     * @apiSuccess {Float}  info.lat  Latitude
     * @apiSuccess {Float}  info.lng  Longitude
     * @apiSuccess {Number} info.type Type.
     * @apiSuccess {String} info.name Name.
     * @apiSuccess {String} info.description Description.
     * @apiSuccess {String} info.image Image.
     * @apiSuccess {Integer} info.create_datetime Create datetime timestamp.
     * @apiSuccess {Array} info.species Species[] species.

     * @apiSuccess {Integer} info.species[].id Species id.
     * @apiSuccess {Integer} info.species[].name Species name.


     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : {},
}
     *
     * @apiUse APIEditError
     */
    public function editAction()
    {
        if ($this->device->device_id == 'undefined') {
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'invalid device';
            return $this->send();
        }
        $id = floatval($this->request->getPost("id", "int", "0"));
        $point = Points::findFirst([
            "conditions" => "id = :pid: AND device_id = :did:",
            "bind" => ["pid"=>$id, "did"=>$this->device->id]
        ]);
        if (!$point) {
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'you can not edit this point';
            return $this->send();
        }

        $lat    = floatval($this->request->getPost("lat", "float", "0.0"));
        $lng    = floatval($this->request->getPost("lng", "float", "0.0"));
        $type   = $this->request->getPost("type", "int", 0);
        $title  = $this->request->getPost("title", "striptags", "");
        $description = $this->request->getPost("description", "striptags", "");
        $species = $this->request->getPost("species", "striptags", "[]");

        $this->logger->log(LOG_INFO,"POINT:: Edit Point...");
        if (isset($_POST['lat']))   $point->lat = $lat;
        if (isset($_POST['lng']))   $point->lng = $lng;
        if (isset($_POST['type']))  $point->type = $type;
        if (isset($_POST['title'])) $point->title = $title;
        if (isset($_POST['description'])) $point->description = $description;
        if (isset($_POST['species'])) $point->species = $species;

        $image = $this->uploadImage();
        if (!empty($image)) {
            $point->image = $image;
        }

        $this->logger->log(LOG_INFO,"POINT:: Edit point...");
        if ($point->save())
        {
            $this->logger->log(LOG_INFO,"POINT:: Point info updated!");
            $point->refresh();
            $info = $point->toArray();

            if (isset($info['species'])) {
                $info['species'] = json_decode($info['species'], true);
            }
            $info['create_datetime'] = $point->create_datetime_ts;
            unset($info['approved']);

            $this->_response['status']  = ResponseStatus::SUCCESS;
            $this->_response['message'] = 'success';
            $this->_response['info'] = $info;
        } else {
            $this->logger->log(LOG_INFO,"POINT:: Point edit error!");
            $this->logger->logModel(LOG_ERR, $point);
            $this->_response['message'] = 'error';
            $this->_response['errors'][]   = 'Point edit error';
            unlink($opath);
        }
        return $this->send();
    }

    /**
     * @apiDescription Get nearby points if lat/lgn passed.<br/>
     * Return 10 last points if lat/lng not passed.</br>
     * Return points with specified type if some "mode" passed...(not implemented)<br/>
     *
     * @api {post} /api/point/list List
     * @apiName PointList
     * @apiGroup API-Point
     * @apiVersion 0.0.1
     *
     * @apiParam {Float} [lat] User latitude
     * @apiParam {Float} [lng] User longitude
     * @apiParam {String} [mode] List mode
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Array} points Point[] Info - (DEPRECATED!!! Use list field)
     * @apiSuccess {Array} list Points array (See PointInfo object structure)

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : {},
}
     *
     * @apiUse APIGlobalError
     */
    public function listAction()
    {
        //PostGis SELECT for distance in meters
        //SELECT ST_DISTANCE(
        //  ST_GeographyFromText('SRID=4326;POINT(46.391304 30.723262)'),
        //  ST_GeographyFromText('SRID=4326;POINT(46.393707 30.723946)'))

        $this->logger->log(LOG_INFO,"POINT:: Get Point List POST[".print_r($_POST, true)."]");

        $lat = $this->request->getPost("lat", "float", "0.0");
        $lng = $this->request->getPost("lng", "float", "0.0");
        $distance = $this->request->getPost("dist", "int", 1000);
        $limit    = $this->request->getPost("limit", "int", 100);

        $lat = floatval($lat);
        $lng = floatval($lng);

        $limit = (int)$limit;
        $distance = (int)$distance;

        $this->logger->log(LOG_INFO,"POINT:: Get Point List
            Lat:[".$lat."] Float[".floatval($lat)."]
            Long:[". $lng."] Float[".floatval($lng)."]"
        );

        $points = [];
        if (($lat <> 0.0) && ($lng <> 0.0))
        {
            /*$this->logger->log(LOG_INFO,"POINT:: Get Points by location...");
            $minlat = $lat - 0.02; $maxlat = $lat + 0.02;
            $minlng = $lng - 0.02; $maxlng = $lng + 0.02;
            $points = Points::find([
                "conditions" => 'lat > :mnlat: AND lat < :mxlat: AND lng > :mnlng: AND lng < :mxlng:',
                "bind" => [
                    "mnlat" => $minlat,
                    "mxlat" => $maxlat,
                    "mnlng" => $minlng,
                    "mxlng" => $maxlng,
                ],
                "order" => 'id DESC',
                "limit" => '50'
            ]);
            /**/

            $str = ''.$lat.' '.$lng;
            $this->logger->log(LOG_INFO,"Current point LatLng string [".$str."]");

            $pnt = new Points();
            $sql =  "
                SELECT * FROM points
                WHERE ST_Distance_Sphere(
                    ST_GeomFromText('POINT(' || lat || ' ' || lng || ')',4326),
                    ST_GeomFromText('POINT(".$lat." ".$lng.")',4326)) < ".$distance."
                ORDER BY id DESC
                LIMIT ".$limit;

            //echo "\n\n".$sql."\n\n";
            $points = new Resultset(null, $pnt, $pnt->getReadConnection()->query($sql));

        } else {
            $points = Points::find([
                "order" => "id DESC",
                "limit" => '10'
            ]);
        }

        $list = $points->toArray();
        foreach ($list as $key => $val)
        {
            unset($list[$key]['user_id']);
            unset($list[$key]['device_id']);
            unset($list[$key]['position']);
            unset($list[$key]['approved']);
            $list[$key]['create_datetime'] = strtotime($val['create_datetime']);
            if (isset($val['species'])) {
                $list[$key]['species'] = json_decode($val['species'], true);
            }
            /*$pnt = [
                'id'  => $val['id'],
                'type'=> $val['type'],
                'lat' => $val['lat'],
                'lng' => $val['lng']
            ];
            $list[$key] = $pnt;*/
        }

        $this->_response['status']  = ResponseStatus::SUCCESS;
        $this->_response['message'] = 'success';
        $this->_response['list'] = $list;
        $this->_response['points'] = $list;

        return $this->send();
    }


    public function deleteAction()
    {
        $this->_response['message'] = 'error';
        $this->_response['errors'][]   = 'Invalid action';
        return $this->send();
    }


    private function uploadImage()
    {
        $image_url ='';
        $temp_dir = $this->config->image->temp_path;
        $dest_dir = $this->config->image->upload_path;
        $dest_url = $this->config->image->upload_url;

        //Print the real file names and their sizes
        $this->logger->log(LOG_INFO,"POINT:: Upload image...");
        if ($this->request->hasFiles())
        {
            $this->logger->log(LOG_INFO,"POINT:: Upload files exists!");
            foreach ($this->request->getUploadedFiles() as $file)
            {
                $file_name = $file->getName();
                $file_size = $file->getSize();
                $this->logger->log(LOG_INFO,"POINT:: Image detected! Name[".$file_name."] Size[". $file_size."]");
                if (empty($file_name) || ($file_size == 0)) {
                    continue;
                }

                $tmp_name = Utils::generateRandom(32);
                $tpath = $temp_dir.$tmp_name.'.jpg';
                $opath = $dest_dir.$tmp_name.'.jpg';

                if ($file->isUploadedFile()) {
                    $this->logger->log(LOG_INFO,"POINT:: File is uploaded throw POST");
                }
                if ($file->moveTo($tpath)) {
                    $this->logger->log(LOG_INFO,"POINT:: File moved to dest [".$tpath."]");
                } else {
                    $this->logger->log(LOG_INFO,"POINT:: Invalid file moveTo dest [".$tpath."]");
                }
                if (file_exists($tpath))
                {
                    $image = new Image($tpath);
                    //Fix image orientation
                    $image->fixOrientation();
                    if ($image->normalizeSize(600, 600) && $image->save($opath))
                    {
                      $this->logger->log(LOG_INFO,"POINT:: Finalfile normalized and saved to [".$opath."]");
                      unlink($tpath);
                      $image_url = $this->basehref.$dest_url.$tmp_name.'.jpg';
                      $this->logger->log(LOG_INFO,"POINT:: Final image URL [".$image_url."]");
                    }
                } else {
                    $this->logger->log(LOG_ERR,"POINT:: File not exists[".$tpath."]");
                }
            }
        } else {
            $this->logger->log(LOG_INFO,"POINT:: No any uploaded files detected!");
        }
        return $image_url;
    }




    /**
     * @apiDescription Get points types
     *
     * @api {post} /api/point/types Types
     * @apiName TypesList
     * @apiGroup API-Point
     * @apiVersion 0.0.1
     *
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Array} list Types array: String[]

     * @apiSuccess {Object} type Type: - Types[] list one item!
     * @apiSuccess {integer} type.id ID
     * @apiSuccess {String} type.name Name
     * @apiSuccess {String} type.icon Icon

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "list" : [],
}
     *
     * @apiUse APIGlobalError
     */
    public function typesAction()
    {
        $types = PointType::getStrings();

        $list = [];
        foreach ($types as $key => $value) {
            if ($key) {
                $list[] = [
                    'id' => $key,
                    'name' => $value,
                    'icon' => 'https://garbage.mg.od.ua/img/types.png'
                ];
            }
        }

        $this->_response['status']  = ResponseStatus::SUCCESS;
        $this->_response['message'] = 'success';
        $this->_response['list'] = $list;
        return $this->send();
    }


    /**
     * @apiDescription Get points species
     *
     * @api {post} /api/point/species Species
     * @apiName SpeciesList
     * @apiGroup API-Point
     * @apiVersion 0.0.1
     *
     *
     * @apiParam {Integer} [type] Species type

     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status
     * @apiSuccess {Array} list Species array: String[]

     * @apiSuccess {Object} type Species: - Species[] list one item!
     * @apiSuccess {integer} type.id ID
     * @apiSuccess {String} type.name Name
     * @apiSuccess {String} type.icon Icon

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "list" : [],
}
     *
     * @apiUse APIGlobalError
     */
    public function speciesAction()
    {
        $type = $this->request->getPost("type", "int", 0);
        $species = Species::getStrings();

        $list = [];
        if ($type == PointType::SEPARATE_POINT ){
            foreach ($species as $key => $value) {
                if ($key) {
                    $list[] = [
                        'id' => $key,
                        'name' => $value,
                        'icon' => 'https://garbage.mg.od.ua/img/species.png'
                    ];
                }
            }
        }
        $this->_response['status']  = ResponseStatus::SUCCESS;
        $this->_response['message'] = 'success';
        $this->_response['list'] = $list;
        return $this->send();
    }

}
