#!/bin/bash
OLD=`grep -oE "[0-9]+" build.php`
BUILD=$(($OLD + 1))
sed "s/%%BUILD%%/$BUILD/g" ./build.tpl > ./build.php
