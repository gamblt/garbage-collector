import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

import { IPoint } from '../interfaces';
import { BaseService } from './base.service';

@Injectable()
export class PointService extends BaseService {

  private _apiUrl = 'api/point';
  private _data: any = [];

  constructor(
    private _http: Http
  ) {
    super();
  }

  add(file: any, point: any): Observable<IPoint>
  {
    console.log('Service: Add Point [%o]', point);
    let formData = new FormData();
    formData.append("image", file);
    for (let key in point) {
      console.log('Add field [%o][%o]', key, point[key]);
      formData.append(key, point[key]);
    }

    let th = this;
    return Observable.create(observer => {
      let xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            let data = JSON.parse(xhr.response);
            //console.log('Uploade response [%o]', data);
            if (data.status == 200){
              observer.next(data.info);
              observer.complete();
            } else {
              observer.error(xhr.response);
            }
          } else {
            observer.error(xhr.response);
          }
        }
      };
      xhr.open('POST', th._apiUrl+'/add', true);
      xhr.send(formData);
    });
  }

  list(params?: any): Observable<IPoint[]>
  {
    console.log('Service: Get Points param[%o]', params);
    let params_hash = this.hash(params);
    if (typeof(this._data[params_hash]) !== 'undefined') {
      console.log('Service: Points cached data...[%o][%o]', params_hash, this._data[params_hash]);
      return Observable.of(this._data[params_hash]);
    }

    console.warn('Service: Get Point list... Params...[%o]', params);
    return this._http.post(this._apiUrl+'/list/', JSON.stringify(params))
      .map((response: Response) => {
        //console.log('Parse petbreeds list...[%o]', response);
        let data = response.json();
        //console.log('Points data...[%o]', data);

        console.log('Service: Save data to cache...[%o]', params_hash);
        this._data[params_hash] = data.list;

        return <IPoint[]> data.list;
      })
      .do(data => {
        //console.log('PPointList: ' +  JSON.stringify(data))
      })
      .catch(this.handleError);
  }
}
