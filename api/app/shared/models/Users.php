<?php

namespace Garbage\Shared\Models;

use Phalcon\Mvc\Model;

class Users extends Model
{
    public function afterFetch()
    {
        $this->create_datetime_ts = strtotime($this->create_datetime);
    }
}
