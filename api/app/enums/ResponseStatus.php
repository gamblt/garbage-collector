<?php
/**
 * Response Statuses Enum
 *
 * @package ResponseStatus
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Enums;

/***/
class ResponseStatus extends \Garbage\Enums\Base\Enum
{
  const GENERAL_ERROR = 500;
  const SUCCESS       = 200;


  const NEED_LOG_IN   = 301;
  const ACCESS_DENIED = 302;

  const LAST_PHONE    = 310;

  const CANT_REPLY    = 320;
  const READ_ONLY     = 321;

  const ALREADY_EXISTS = 600;
  const ALREADY_VOTED  = 601;

  /**
   * @copydoc
   */
  public static function getStrings()
  {
    return [];
  }
}
