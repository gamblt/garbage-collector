<?php
/**
 * PointType Enum
 *
 * @package PointType
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2017 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Enums;
/***/
class PointType extends \Garbage\Enums\Base\Enum
{
  const UNDEFINED       = 0;
  const STATIC_TRASH    = 1;
  const MOVABLE_TRASH   = 2;
  const SEPARATE_TRASH  = 3;
  const SEPARATE_POINT  = 4;

  /**
   * @copydoc
   */
  public static function getStrings()
  {
    return [
      self::UNDEFINED       => 'Неизвестно',
      self::STATIC_TRASH    => 'Стационарная мусорка возле заведения',
      self::MOVABLE_TRASH   => 'Альфатер, переносной',
      self::SEPARATE_TRASH  => 'Урна для раздельного сбора мусора',
      self::SEPARATE_POINT  => 'Пункт раздельного сбора мусора',
      /**
       * @todo Подумать как разделять мусорки и пункты по приему бумаги/пластика/батареек и прочего добра...
      */
    ];
  }
}
