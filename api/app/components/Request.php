<?php
/**
 * @package Logger;
 */

namespace Garbage\Components;


/***/
class Request extends \Phalcon\Http\Request
{

    private $_sanitizer;

    function __construct() {
        //parent::__construct();
        $this->_sanitizer = new Sanitizer();
    }

    /**
     * Return sanitized Value
     * False could be passed as default value
     * @param type $name
     * @return type
     */
    public function getPostSanitized($name, $default = '')
    {
        $data = $this->getPost($name, null, $default);
        if (!empty($data)) {
            return $this->_sanitizer->sanitize($data);
        }
        return $data;
    }

}