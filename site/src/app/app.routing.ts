import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { ProfileComponent } from './profile/profile.component';
import { AddPointComponent } from './add-point/add-point.component';
import { NotFoundComponent } from './not-found/not-found.component';
//import { AuthService, OverlayService }  from './services';

const appRoutes: Routes = [
  {path: '', component: MapComponent},
  {path: 'addpoint', component: AddPointComponent},
  {path: 'profile', component: ProfileComponent},
  {path: '**', component: NotFoundComponent} //Default (not-found) router,
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
