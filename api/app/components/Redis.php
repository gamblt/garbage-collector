<?php
/**
 * @package Redis;
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

/**
 * Redis Access
 * @todo move to native gettext to make simple domain translation
 */
class Redis extends Component
{
    private $_instance;

    private $_expired = 3600;
    /**
     * Translate constructor
     * @param type $config
     * @return type
     */
    function __construct()
    {
        $config = $this->config->redis;
        try {
            $this->_instance = new \Redis();
            $this->_instance->connect(
                $config->host,
                $config->port,
                $config->timeout
            );
            $this->_instance->auth(
                $config->password
            );
            $this->_instance->select(
                $config->db
            );
            $this->_instance->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
            $this->_instance->setOption(\Redis::OPT_PREFIX, $config->prefix.":"); // use custom prefix on all keys
        }
        catch (Exception $e) {
            //Use direct sysog to prevent recursive erors by Logger collector
            $this->logger->log(LOG_ERR,"Redis Connection error: [".$e->getMessage()."]");
        }
    }

    /**
     * Forward all undefined calls to \Redis Instance;
     * @param type $method
     * @param type $args
     * @return type
     */
    function __call($method, $args)
    {
        return call_user_func_array(array($this->_instance, $method), $args);
    }


    public function getInstance()
    {
        return $this->_instance;
    }
}
