<?php

//Registering a router
$di->set('router', function(){

    $router = new \Phalcon\Mvc\Router(false);

    $router->removeExtraSlashes(true);

    $router->setDefaultModule("site");

    //$router->setDefaults(array('controller' => 'index', 'action' => 'notfound'));


    $router->notFound(array(
        "module"    => "api",
        "controller" => "index",
        "action"     => "notfound"
    ));


    $router->add('/', array(
        'module'        => 'api',
        'controller'    => 'index',
        'action'        => 'index',
    ));

    $router->add('/:action', array(
        'module'        => 'api',
        'controller'    => 'index',
        'action'        => 1,
    ));

    $router->add("/api/:controller/:action", array(
        'module'        => 'api',
        'controller'    => 1,
        'action'        => 2,
    ));

    $router->add("/api/:controller/:action/:params", array(
        'module'        => 'api',
        'controller'    => 1,
        'action'        => 2,
        'params'        => 3
    ));



    $router->add('/api/admin/:action', array(
        'module'        => 'admin',
        'controller'    => 'index',
        'action'        => 1,
    ));

    $router->add("/api/admin/:controller/:action", array(
        'module'        => 'admin',
        'controller'    => 1,
        'action'        => 2,
    ));

    $router->add("/api/admin/:controller/:action/:params", array(
        'module'        => 'admin',
        'controller'    => 1,
        'action'        => 2,
        'params'        => 3
    ));


    $router->add('/api/sessioncreate', array(
        'module'        => 'api',
        'controller'    => 'session',
        'action'        => 'create',
    ));
    $router->add('/api/sessiondestroy', array(
        'module'        => 'api',
        'controller'    => 'session',
        'action'        => 'destroy',
    ));
    $router->add('/api/success', array(
        'module'        => 'api',
        'controller'    => 'session',
        'action'        => 'success',
    ));

    $router->add('/logout', array(
        'module'        => 'api',
        'controller'    => 'session',
        'action'        => 'logout',
    ));

    return $router;
});
