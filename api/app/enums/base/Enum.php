<?php
/**
 * Base Enum Class
 * @package default
 */

namespace Garbage\Enums\Base;

/**
 * extends \Phalcon\Di\Injectable //To pass translator depency
 */
class Enum
{
  /**
   * Get All enum titles
   *
   * @return array
   */
  public static function getStrings()
  {
    return [];
  }
  /**
   * Get enum title
   *
   * @param int $id Enum value
   *
   * @return string
   */
  public static function getNameByID($id)
  {
    $list = static::getStrings();
    $id = (int)$id;
    return (isset($list[$id])) ? $list[$id] : "Undefined";
  }
}
