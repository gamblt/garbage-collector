import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';

declare var google:any;
declare var navigator:any;

@Injectable()
export class LocationService
{
  public locationUpdated$: Observable<any>;
  private _locationObserver: Observer<any>;
  private _location: google.maps.LatLng;
  private _watchPositionId: any;

  constructor()
  {
    this.locationUpdated$ = new Observable(observer => {
      this._locationObserver = observer;
    }).share();

    let th = this;
    if (navigator.geolocation)
    {
      console.log('Get position...');
      navigator.geolocation.getCurrentPosition((position) =>
      {
        th._location = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );
        console.log('Current position COOR[%o] LOC[%o]', position.coords, th._location);
        th._locationObserver && th._locationObserver.next(th._location);
      });


      //setTimeout(function()
      //{
        th._watchPositionId = navigator.geolocation.watchPosition(
        function(location) {
          console.log('Watch Position... [%o]', location.coords);
          th._location = new google.maps.LatLng(
            location.coords.latitude,
            location.coords.longitude
          );
          th._locationObserver && th._locationObserver.next(th._location);
        },
        function(error) {
          console.log('Track position error! [%o]', error);
        },{
            timeout : 500,
            enableHighAccuracy : true,
            maximumAge : 200
        });
      //},0);
    } else {
      console.error('Geolocation is not supported!');
    }
  }

  getLocation() {
    return this._location;
  }
}
