<?php
/**
 * API Module
 * Content All API requests
 *
 * @package APIModule
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\API;

/***/
class Module
{
    public function registerAutoloaders()
    {
        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces(array(
            'Garbage\API\Controllers' => '../app/modules/api/controllers/'
            //'Garbage\API\Models'     => '../app/modules/api/models/'
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them module-specific
     */
    public function registerServices($di)
    {
        //Registering a dispatcher
        $di->set('dispatcher', function ()
        {
            //Redirect af Action or controller not found!
            $eventsManager = new \Phalcon\Events\Manager();
            $eventsManager->attach('dispatch:beforeException', function($event, $dispatcher, $exception)
            {
                switch ($exception->getCode())
                {
                    case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(array(
                            'module'=>'api',
                            'controller'=>'index',
                            'action'=>'notfound'
                        ));
                        return false;
                }
            });

            //Bind controllers
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
            $dispatcher->setEventsManager($eventsManager);
            $dispatcher->setDefaultNamespace("Garbage\API\Controllers\\");
            return $dispatcher;
        });
    }
}
