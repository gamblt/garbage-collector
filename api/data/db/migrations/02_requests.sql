-- Table: public.requests

DROP TABLE IF EXISTS public.requests;

CREATE TABLE public.requests
(
  id bigserial,
  ip character varying,
  url character varying,
  device_id character varying,
  referer character varying,
  get jsonb DEFAULT '{}'::jsonb,
  post jsonb DEFAULT '{}'::jsonb,
  useragent character varying,
  useragent_hash character varying,
  create_datetime timestamp with time zone DEFAULT now(),
  CONSTRAINT requests_id_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.requests
  OWNER TO garbage;
