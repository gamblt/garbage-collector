<?php
/**
 * @package CurlSender;
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

/***/
class CurlSender extends Component
{
    public function sendRequest(
        $method, $url, $data, 
        $headers = [], $auth = [], $is_rest = false
    ){

        $response = [
            'status'    => false,
            'http_code' => 0,
            'content'   => '',
            'curl_info' => [
                'method'    => $method,
                'url'       => $url,
                'headers'   => $headers,
                'isrest'    => $is_rest,
                'auth_creadentials' => $auth
            ]
        ];

        $curl = curl_init();
        $this->logger->log(LOG_INFO,'CSND:: Curl POST requester entry for url ['.$url.']');

        //Set curl extra-headers
        if (!empty($headers))
        {
            $this->logger->log(LOG_INFO,"CSND:: Add extra-headers...\n[".print_r($headers, true)."]");
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        //Add Basic authentication
        if (!empty($auth['credentials']) && !empty($auth['type']))
        {
            $this->logger->log(LOG_INFO,"CSND:: Add basic authentication... [".$auth_type."][".$auth_credentials."]");
            curl_setopt($curl, CURLOPT_HTTPAUTH, $auth_type);
            curl_setopt($curl, CURLOPT_USERPWD, $auth_credentials);
        }

        $method = strtolower($method);

        switch ($method) {
            case 'post':
                $this->logger->log(LOG_INFO,"CSND:: POST request!");
                $this->logger->log(LOG_INFO,"CSND:: Add post data...\n[".print_r($data, true)."]");

                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;

            case 'get':
                $this->logger->log(LOG_INFO,"CSND:: GET request!");
                if (!empty($data))
                {
                    $this->logger->log(LOG_INFO,"CSND:: Prepare get params...");
                    $url = $url."?".http_build_query($data);

                }
                break;

            case 'patch':
                $this->logger->log(LOG_INFO,"CSND:: PATCH request!");
                $this->logger->log(LOG_INFO,"CSND:: Add post data...\n[".print_r($data, true)."]");
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;

            default:
                curl_close($curl);
                $this->logger->log(LOG_ERR,"CSND:: Invalid request method [".$method."]");
                return $response;
                break;
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
        //curl_setopt($curl, CURLOPT_ENCODING, 'deflate');

        //Animal-id has different site on IPV6 so force IPV4 resolving
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); 

        //curl_setopt($curl, CURLOPT_USERAGENT, 'MoyGorod CurlSender (+https://mg.od.ua/)');

        //Validate SSL Sertificate
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); //1
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); //2

        $response['content']  = curl_exec($curl);
        $response['http_code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $serverIP = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : "";
        $remoteIP = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";

        //All responce code is ok if REST requests!
        if ((($response['http_code'] >= 200) && ($response['http_code'] < 300)) || $is_rest)
        {
            if (empty($response['content']))
            {
                $fullinfo = curl_getinfo($curl);
                $this->logger->log(LOG_INFO, "CSND:: Content is empty!\n
                    ServerIP:[" . $serverIP . "]\n RemoteIP:[" . $remoteIP . "]\n HTTP_Status[" . $response['http_code'] . "]\n
                    CurlInfo [\n" . print_r($fullinfo, true) . "\n");
            }
            else
            {
                $this->logger->log(LOG_INFO, 'CSND:: INFO: HTTP_Status:[' . $response['http_code'] . ']: Content received. Len:['.strlen($response['content']).']');
                $response['status'] = 'success';
            }
        }
        else
        {
            $fullinfo = curl_getinfo($curl);

            $this->logger->log(LOG_ERR, "CSND:: ERROR: HTTP HTTP_Status! [" . $response['http_code'] . "],\n
                    IP[" . $serverIP . "],\n\n
                    CurlInfo [\n" . print_r($fullinfo, true) . "]\n\n
                    ContentLen[" . count($response['content']) . "]\n\n\n,
                    Content[
                    " . $response['content'] . "]
                    \n\n\n\n");
        }
        curl_close($curl);
        return $response;
    }
}
