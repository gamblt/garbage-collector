var gulp        = require('gulp');
var apidoc      = require('gulp-apidocjs');

gulp.task('default', ['apidoc', 'watch']);

//Generate documentation for API module
gulp.task('apidoc', function (cb) {
    apidoc.exec({
        src: "../api/app/modules/api/",
        dest: "../apidocs/",
        debug: true,
        includeFilters: [ ".*\\.php$" ]
    }, cb);
});

gulp.task('watch', function () {
    gulp.watch('../api/app/modules/api/**/*Controller.php', ['apidoc']);
});