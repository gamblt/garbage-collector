-- Table: public.users

DROP TABLE IF EXISTS public.users;

CREATE TABLE public.users
(
  id bigserial,
  email character varying,
  phone character varying,
  first_name character varying,
  last_name character varying,
  nick_name character varying,
  avatar character varying,
  approved boolean DEFAULT false,
  create_datetime timestamp with time zone DEFAULT now(),
  CONSTRAINT users_id_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.users
  OWNER TO garbage;
