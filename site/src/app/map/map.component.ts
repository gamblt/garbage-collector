import { Component, OnInit, ElementRef, NgZone } from '@angular/core';

import { LocationService, PointService } from '../services';
import { IPoint } from '../interfaces';

declare var $:any;
declare var google:any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit
{
  private map: google.maps.Map;
  private marker: google.maps.Marker;
  private point_marker: google.maps.Marker;
  private markers: any = [];

  private currentLocation:google.maps.LatLng;

  public activePoint: IPoint;

  public newpoint: any = {
    lat:0,
    lng:0,
    title:"",
    description:""
  };
  imagefile: any = '';
  private _file: any;
  public showaddpointdialog: boolean = false;

  constructor(
    private zone: NgZone,
    private _elementRef: ElementRef,
    private _pointService: PointService,
    private _locationService: LocationService
  ) {
    this.currentLocation = new google.maps.LatLng(46.47925, 30.744498);
  }

  ngOnInit()
  {
    this.currentLocation = this._locationService.getLocation();

    this.map = new google.maps.Map(document.getElementById('map'), {
      center: this.currentLocation,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      disableDoubleClickZoom: false,
      draggable: true,
      scrollwheel: false,
      /*styles: [{
        featureType:"poi",
        stylers: [
          //{ visibility: "off"}
          { color: '#DFD2AE' }
        ]
      }]*/
    });

    this.marker = new google.maps.Marker({
      clickable : false,
      icon : new google.maps.MarkerImage(
        '//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
        new google.maps.Size(22, 22),
        new google.maps.Point(0, 18),
        new google.maps.Point(11, 11)
      ),
      shadow : null,
      zIndex : 999,
      title : 'me',
      map : this.map
    });



    this._locationService.locationUpdated$.subscribe((location: google.maps.LatLng) =>
    {
      console.log('New location...[%o][%o]',location.lat(), location.lng());
      this.currentLocation = location;
      this.marker.setPosition(location);
      this.map.panTo(location);


      console.log('Get nearest points...');
      this._pointService.list({
        lat: location.lat(),
        lng: location.lng()
      }).subscribe((list) => {
        console.log('POINTS LIST [%o]', list);
        this.updateMarkers(list);
      })
    });
  }

  /* clear all proint from map */
  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].marker.setMap(null);
    }
    this.markers = [];
  }

  /* Show point on map */
  updateMarkers(points: IPoint[]) {
    console.log('Show points...[%o]', points);
    for (var i = 0; i < points.length; i++) {
      console.warn('Add new marker [%o][%o]', points[i].lat, points[i].lng)
      this.addMarker(points[i], new google.maps.LatLng(
        points[i].lat,
        points[i].lng
      ), i*200);
    }
  }

  followMe() {
    console.log('FollowMe click!');
  }

  private addMarker(point, position, timeout) {
    let th = this;
    let ts = this.getTime();
    window.setTimeout(function() {
      if (typeof(th.markers[point.id]) == 'undefined')
      {
        th.markers[point.id] = {
          'time': ts,
          'info': point,
          'marker': new google.maps.Marker({
            //draggable: true, //debug
            position: position,
            map: th.map,
            icon : th.getMarkerIcon(point.type),
            animation: google.maps.Animation.DROP
          })
        }

        th.markers[point.id].marker.addListener('click', function(event) {
          //console.log('Marker Event[%o] Point[%o]', event, point);
          th.showPointInfo(point);
        });

      } else {
        th.markers[point.id].time = ts;
        console.warn('Point [%o] already exists', th.markers[point.id]);
      }
    }, timeout);
  }


  private getMarkerIcon(type)
  {
    return new google.maps.MarkerImage(
      'http://findicons.com/files/icons/1580/devine_icons_part_2/128/trash_recyclebin_empty_closed.png',
      new google.maps.Size(128, 128),
      new google.maps.Point(0, 0),
      new google.maps.Point(11, 22),
      new google.maps.Size(22, 22),
    );
  }

  private showPointInfo(point) {
    console.log('Point INFO[%o]', point);
    this.zone.run(() => {
      this.activePoint = point;
    });
  }

  //Close cropper
  public closePointInfoModal() {
    this.activePoint = null;
  }

  private getTime() {
    return Math.floor(Date.now() / 1000);
  }



  /**
   * Open AddPointDialog form
   */
  addPoint()
  {
    console.log('Add point...');
    this.point_marker = new google.maps.Marker({
      map : this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: this.currentLocation,
      clickable : true
    });
    let th = this;
    this.point_marker.addListener('click', function(){
      th.openAddMarkerDialog(th.point_marker);
    });
  }

  openAddMarkerDialog(marker)
  {
    console.log('Open add marker dialog! Marker[%o]', marker);
    console.log('Open add marker dialog! Position Lat[%o] Lng[%o]', marker.internalPosition.lat(), marker.internalPosition.lng());
    this.showaddpointdialog = true;

    //Reset form
    this.newpoint= {
      lat:         marker.internalPosition.lat(),
      lng:         marker.internalPosition.lng(),
      title:       "",
      description: ""
    };
    let form: any = $('#addpointform')[0];
    if (form) {
      form.reset();
    }
  }


  //Close cropper
  closeAddMarkerDialog() {
    this.showaddpointdialog = false;
  }

  selectImage(event)
  {
    this._file = event.target.files[0];
    this.imagefile = '';
  }
    /**
   * Open AddPoint form
   */
  addPointProcess() {
    console.log('Add point process...');
    let add_subs = this._pointService
    .add(this._file, this.newpoint)
    .subscribe(info => {
      console.log("Add point... [%o]", info);
      if(add_subs) add_subs.unsubscribe();
      this.showaddpointdialog = false;
    });
  }
}
