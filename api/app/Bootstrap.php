<?php
/**
 * Bootstrap
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 * @author Aleksandr Shkarbaliuk (Gambit)
 */

namespace Garbage;

use \Phalcon\Mvc\View;
use \Phalcon\Mvc\Application;
use \Phalcon\DI\FactoryDefault;
use \Phalcon\Assets\Manager;

use \Garbage\Components\Request;
use \Garbage\Components\Security;
use \Garbage\Components\Activity;


use Phalcon\Events\Manager as EventsManager;

error_reporting(E_ALL);

/***/
class Bootstrap
{
    /**
     * Main bootstrap function
     */
    public function run()
    {

        mb_internal_encoding("UTF-8");

        // Register namespaces
        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces([
            'Garbage\Components'    => __DIR__.'/components/',
            'Garbage\Config'        => __DIR__.'/config/',
            'Garbage\Enums'         => __DIR__.'/enums/',
            'Garbage\Enums\Base'    => __DIR__.'/enums/base/',
            'Garbage\Shared\Models' => __DIR__.'/shared/models/'
        ]);
        $loader->registerDirs([
            __DIR__.'/components/',
            __DIR__.'/config/',
            __DIR__.'/enums/',
            __DIR__.'/enums/base'
        ]);
        $loader->register();


        $di = new FactoryDefault();

        $di->set("request", function(){
            $request = new Request();
            return $request;
         }, true);

        $info = $this->getRequestInfo();
        $di->setShared("basehref", function() use ($info) {
            return $info['scheme']."://".$info['full_domain'];
        });

        // Get config scoped in subdomain
        $request_info = $this->getRequestInfo();
        $config = \Garbage\Config\Config::get($request_info['city_subdomain']);
        $di->set('config', $config);

        $di->setShared('sanitizer', function () {
            return new \Garbage\Components\Sanitizer();
        });
        
        $di->setShared('redis', function () {
            return new \Garbage\Components\Redis();
        });

        $di->setShared('cookies', function () {
            return new \Garbage\Components\Cookies();
        });

        $di->setShared('curl', function () {
            return new \Garbage\Components\CurlSender();
        });

        $cache = new \Garbage\Components\RedisCache($config->cache->lifetime);
        $di->setShared('cache', $cache);

        $logger = new \Garbage\Components\Logger($request_info['city_subdomain']);
        $di->setShared('logger',$logger);


        require __DIR__ . '/bootstrap/router.php';

        // Init session
        //Don't set domain! If domain has was set the dot has been prepended and all subdomains has been applied
        //session_set_cookie_params ($lifetime, $path, $domain, $secure, $httponly)
        $sess = $config->get('session');
        session_set_cookie_params($sess->ttl, '/', '.garbage.mg.od.ua', $sess->secure, true);
        $mysession = new \Garbage\Shared\Models\Session();
        $mysession->startSession();


        // Create an Events Manager
        /*$eventsManager = new EventsManager();
        // Attach the listener to the EventsManager
        $eventsManager->attach('global', new GlobalListener());
        $di->setShared('events', $eventsManager);*/


        $di->setShared('assets', function () {
            return new Manager();
        });

        $di->set('view', function () use ($logger) {
            $view = new \Phalcon\Mvc\View();
            return $view;
        });

        $db = new \Phalcon\Db\Adapter\Pdo\Postgresql(array(
            "host"      => $config->database->host,
            "port"      => $config->database->port,
            "username"  => $config->database->username,
            "password"  => $config->database->password,
            "dbname"    => $config->database->dbname
        ));
        $di->set('db', $db);

        $application = new Application($di);

        //Register the installed modules
        $application->registerModules(array(
            'api' => array(
                'className' => 'Garbage\API\Module',
                'path' => __DIR__.'/modules/api/Module.php'
            ),
            'admin' => array(
                'className' => 'Garbage\Admin\Module',
                'path' => __DIR__.'/modules/admin/Module.php'
            ),
        ));

        echo $application->handle()->getContent();

    }



    protected function getRequestInfo()
    {
        // SEt secure cookie for all subdomains
        $request = new Request();
        $origin_domain =  $request->getHttpHost();
        $uri =  $request->getURI();
        $domain_array = explode('.', $origin_domain);
        // DEfault City;
        $city = 'garbage';
        $domain = implode('.',$domain_array);

        return [
            'scheme'      => $request->getScheme(),
            'full_domain' => $origin_domain,
            'main_domain' => $domain,
            'city_subdomain' => $city,
            'uri' => $uri
        ];
    }
}
