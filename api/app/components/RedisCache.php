<?php
/**
 * Redis cache realization
 *
 * @package RedisCache;
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;


class RedisCache extends Component
{
    /**
     * Redis instance
     * @var \Redis
     */
    private $_instance;

    /**
     * Cache lifetime in sec
     * @var int
     */
    protected $_ttl = 86400;

    /**
     * Initialize and Configure Redis Cache
     * @param object $config
     * @return RedisCache
     */
    function __construct($ttl = 0)
    {
        $config = $this->config->redis;
        if ($ttl > 0) {
            $this->_ttl = $ttl;
        }
        try {
            $this->_instance = new \Redis();
            $this->_instance->connect(
                $config->host,
                $config->port,
                $config->timeout
            );
            $this->_instance->auth(
                $config->password
            );
            $this->_instance->select(
                $config->db
            );
            $this->_instance->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
            // use custom prefix on all keys
            $this->_instance->setOption(\Redis::OPT_PREFIX, $config->prefix.":cache:");
        }
        catch (Exception $e) {
            //Use direct sysog to prevent recursive erors by Logger collector
            syslog(LOG_ERR,"Redis Connection error: [".$e->getMessage()."]");
        }
    }

    /**
     * Set cache
     *
     * @param string key   / hash
     * @param string value / key
     * @param string       / value
     *
     * @return bool
     */
    public function set()
    {
        switch (func_num_args()) {
            case 2: {
                //Base save
                return $this->_instance->setex(
                    func_get_arg(0),
                    $this->_ttl,
                    func_get_arg(1)
                );
                break;
            }
            case 3: {
                //hash save
                return $this->_instance->hSet(
                    func_get_arg(0),
                    func_get_arg(1),
                    func_get_arg(2)
                ) && $this->_instance->expire(
                    func_get_arg(0),
                    $this->_ttl
                );
                break;
            }
            default: {
            }
        }
        return false;
    }

    /**
     * Get cache
     *
     * @param string key / hash
     * @param string     / key
     *
     * @return string|false
     */
    public function get()
    {
        switch (func_num_args()) {
            case 1: {
                //Base get
                return $this->_instance->get(func_get_arg(0));
                break;
            }
            case 2: {
                //Hash Get
                return $this->_instance->hGet(func_get_arg(0), func_get_arg(1));
                break;
            }
            default: {
            }
        }
        return false;
    }

    /**
     * Delete cache
     *
     * @param string key / hash
     * @param string     / key
     *
     * @return bool
     */
    public function del()
    {
        switch (func_num_args()) {
            case 1: {
                //Base get
                return $this->_instance->delete(func_get_arg(0));
                break;
            }
            case 2: {
                //Hash Get
                return $this->_instance->hDel(func_get_arg(0), func_get_arg(1));
                break;
            }
            default: {
            }
        }
        return false;

    }
}