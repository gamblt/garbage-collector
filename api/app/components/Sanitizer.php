<?php
/**
 * HTMLPurifier wrapper to Sanitize values
 *
 * @package Sanitizer;
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

require_once(__DIR__.'/../../vendor/htmlpurifier-4.7.0/library/HTMLPurifier.auto.php');

/***/
class Sanitizer extends Component
{
    /**
     * HTMLPurifier incstance
     * @var \HTMLPurifier
     */
    private $_purifier;

    /**
     * Description
     * @return \HTMLPurifier
     */
    function __construct()
    {

        $config = \HTMLPurifier_Config::createDefault();

        $config->set('HTML.AllowedElements', array('a','b','p','i','em','u', 'br', 'div', 'img', 'span', 'ul','ol', 'li', 'strong','iframe','h1','h2','h3','h4','h5','h6','table','thead','tbody','tr','th','td'));

        /**
         * @todo Make style attibute allow only for admin!!! to prevent hijacking, form override
         */
        $config->set('HTML.AllowedAttributes', [
          'a.href', 'a.target', 'img.src', 
          '*.alt', '*.title', '*.border', 
          'a.target', 'a.rel', 'iframe.src', 
          '*.class', '*.style', 
          'iframe.frameborder', 'iframe.allowfullscreen', 
          'allowfullscreen', 'width','height',
          //'img.url', 'img.pid', 'img.mg-box', 'img.mg-list', 'img.style', 
          'table.cellspacing','table.cellpadding','table.align','table.dir'
        ]);

        //Fonts Allowed
        //$config->set('CSS.AllowedFonts', ['Roboto', 'Roboto Slab']);

        //A-tag configs
        $config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
        //Iframe configs
        $config->set('HTML.SafeIframe', true);
        $config->set('CSS.AllowTricky', true); // allow style display:block
        $config->set('URI.SafeIframeRegexp', '%^https://(www.youtube.com/embed/|player.vimeo.com/video/)%');

        $def = $config->getHTMLDefinition(true);
        $def->addAttribute('iframe', 'allowfullscreen', 'Enum#allowfullscreen,');
        $def->addAttribute('img', 'mg-box', 'CDATA');
        $def->addAttribute('img', 'mg-list', 'CDATA');
        $def->addAttribute('img', 'pid', 'Number');
        $def->addAttribute('img', 'url', 'CDATA');
        $def->addAttribute('img', 'style', 'CDATA');

        //Typelist: http://htmlpurifier.org/docs/enduser-customize.html
        //$def->addElement('mg-box', 'Inline', 'Inline', 'Common');
        //$def->addAttribute('mg-box', 'url', 'CDATA');
        //$def->addElement('mg-list', 'Inline', 'Inline', 'Common');
        //$def->addAttribute('mg-list', 'pid', 'Number');

        // add some custom CSS3 properties
        $css_definition = $config->getDefinition('CSS');

        $border_radius =
        $info['border-top-left-radius'] =
        $info['border-top-right-radius'] =
        $info['border-bottom-left-radius'] =
        $info['border-bottom-right-radius'] =
        new \HTMLPurifier_AttrDef_CSS_Composite(array(
            new \HTMLPurifier_AttrDef_CSS_Length('0'),
            new \HTMLPurifier_AttrDef_CSS_Percentage(true)
        ));

        $info['border-radius'] = new \HTMLPurifier_AttrDef_CSS_Multiple($border_radius);

        $info['max-width'] =
        $info['max-height'] = new \HTMLPurifier_AttrDef_CSS_Length('0');
      //$info['display'] = new HTMLPurifier_AttrDef_CSS_Multiple($border_radius);

      // wrap all new attr-defs with decorator that handles !important
      $allow_important = $config->get('CSS.AllowImportant');
      foreach ($info as $k => $v) {
        $css_definition->info[$k] = new \HTMLPurifier_AttrDef_CSS_ImportantDecorator($v, $allow_important);
      }


        $this->_purifier = new \HTMLPurifier($config);
    }

    /**
     * Sanitize input HTML
     * @param string $dirty_html Dirty HTML
     * @return string Clear HTML
     */
    public function sanitize($dirty_html)
    {
        return $this->_purifier->purify($dirty_html);
    }
}
