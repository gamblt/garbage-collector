<?php
/**
 * Activity Spaces Enum
 *
 * @package ActivitySpace
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */

namespace Garbage\Enums;

/***/
class ActivitySpace extends \Garbage\Enums\Base\Enum
{
  const UNKNOWNSPACE  = 0;
  const USERSPACE     = 1;
  const ADMINSPACE    = 2;

  /**
   * @copydoc
   */
  public static function getStrings()
  {
    return [
      self::UNKNOWNSPACE  => 'Unauthorized',
      self::USERSPACE     => 'UserSpace',
      self::ADMINSPACE    => 'AdminSpace',
    ];
  }
}
