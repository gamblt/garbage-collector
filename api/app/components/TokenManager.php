<?php
/**
 * @package TokenManager;
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

use Garbage\Components\Utils;

/**
 * TokenManager
 */
class TokenManager extends Component
{
    private $_instance; //Redis instance to save tokens

    private $_expired = 3600;
    /**
     * Translate constructor
     * @param type $config
     * @return type
     */
    function __construct()
    {
        $config = $this->config->tokenmanager;
        try {
            $this->_instance = new \Redis();
            $this->_instance->connect(
                $config->host,
                $config->port,
                $config->timeout
            );
            $this->_instance->auth(
                $config->password
            );
            $this->_instance->select(
                $config->db
            );
            $this->_instance->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
            $this->_instance->setOption(\Redis::OPT_PREFIX, $config->prefix.":"); // use custom prefix on all keys
        }
        catch (Exception $e) {
            //Use direct sysog to prevent recursive erors by Logger collector
            $this->logger->log(LOG_ERR,"Redis Connection error: [".$e->getMessage()."]");
        }
    }

    /**
     * Forward all undefined calls to \Redis Instance;
     * @param type $method
     * @param type $args
     * @return type
     */
    function __call($method, $args)
    {
        return call_user_func_array(array($this->_instance, $method), $args);
    }


    public function getInstance()
    {
        return $this->_instance;
    }


    public function createToken($type = 'Security', $data, $ttl = 0, $ever = false)
    {
        $ttl = (int)$ttl;
        $this->logger->log(LOG_INFO, "Create token [".$type."][".$data."]");

        $time = $this->_expired;
        if ($ever) {
            $time = 31536000;
        } elseif ($ttl > 0) {
            $time = (int)$ttl;
        }

        $token = hash('sha256', Utils::generateRandom(1024));
        $key = "Tokens:".ucfirst($type).":".$token;

        $this->logger->log(LOG_INFO, "Token  TTL [".($time*1000)."]");

        // Strange bug but setEx doesn't work correctly!!!
        if ($this->_instance->set($key, $data)
            &&
            $this->_instance->setTimeout($key, $time*1000)
        ) {
            $this->logger->log(LOG_INFO, "Created token [".$token."]");
            return $token;
        } else {
            $this->logger->log(LOG_INFO, "Created token error!!! TKey[".$key."]");
        }
        return false;
    }

    public function getTokenInfo($type = 'Security', $token)
    {
        $this->logger->log(LOG_INFO, "Get token info:[".$type."][".$token."]");
        $key = "Tokens:".ucfirst($type).":".$token;
        $info = $this->_instance->get($key);
        $this->logger->log(LOG_INFO, "Info:[".print_r($info, true)."]");
        return $info;
    }

    public function deleteToken($type = 'Security', $token)
    {
        $this->logger->log(LOG_INFO, "Delete token [".$type."]");
        $key = "Tokens:".ucfirst($type).":".$token;
        return $this->_instance->delete($key);
    }

}
