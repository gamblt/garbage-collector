<?php
/**
 * Profile API Controller
 *
 * @package APIProfileController
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\API\Controllers;

use Phalcon\Mvc\Controller;

use Garbage\Components\Image;
use Garbage\Components\Utils;

use Garbage\Shared\Models\Users;

use Garbage\Enums\ResponseStatus;

/***/
class ProfileController extends CRUDController
{
    protected $_model_name = 'Users';

    /**
     * @apiDescription Add new profile for device
     *
     * @api {post} /api/profile/add Add
     * @apiName ProfileAdd
     * @apiGroup API-Profile
     * @apiVersion 0.0.1
     *
     * @apiParam {String} [first_name] Owner first name
     * @apiParam {String} [last_name] Owner last name
     * @apiParam {String} [nick_name] Owner nick name
     * @apiParam {String} [email] Owner email
     * @apiParam {String} [phone] Owner phone
     * @apiParam {File} [avatar] Owner avatar
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status

     * @apiSuccess {Object} info Owner Info
     * @apiSuccess {Number} info.id   Id.
     * @apiSuccess {String} info.first_name First name.
     * @apiSuccess {String} info.last_name Last name.
     * @apiSuccess {String} info.nick_name Nick name.
     * @apiSuccess {String} info.email Email.
     * @apiSuccess {String} info.phone Pho.ne
     * @apiSuccess {String} info.avatar Avatar.

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : {},
}
     *
     * @apiUse APIAddError
     */
    public function addAction()
    {
        if ($this->device->device_id == 'undefined') {
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'invalid device';
            return $this->send();
        }
        if (!empty($this->device->user_id)) {
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'account already exists for this device';
            return $this->send();
        }
        $user = new Users();
        $user->first_name = $this->request->getPost("first_name", "striptags", "");
        $user->last_name  = $this->request->getPost("last_name", "striptags", "");
        $user->nick_name  = $this->request->getPost("nick_name", "striptags", "");
        $user->email      = $this->request->getPost("email", "striptags", "");
        $user->phone      = $this->request->getPost("phone", "striptags", "");
        $user->avatar     = $this->uploadAvatar();
        if ($user->save())
        {
            $user->refresh();
            $this->device->user_id = $user->id;
            $this->device->save();

            $user_info = $user->toArray();
            unset($user_info['create_datetime']);
            unset($user_info['approved']);

            $this->_response['status']  = ResponseStatus::SUCCESS;
            $this->_response['message'] = 'success';
            $this->_response['info'] = $user_info;
        } else {
        }
        return $this->send();
    }

    /**
     * @apiDescription Edit profile for device
     *
     * @api {post} /api/profile/edit Edit
     * @apiName ProfileEdit
     * @apiGroup API-Profile
     * @apiVersion 0.0.1
     *
     * @apiParam {String} [first_name] Owner first name
     * @apiParam {String} [last_name] Owner last name
     * @apiParam {String} [nick_name] Owner nick name
     * @apiParam {String} [email] Owner email
     * @apiParam {String} [phone] Owner phone
     * @apiParam {File} [avatar] Owner avatar
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status

     * @apiSuccess {Object} info Owner Info
     * @apiSuccess {Number} info.id   Id.
     * @apiSuccess {String} info.first_name First name.
     * @apiSuccess {String} info.last_name Last name.
     * @apiSuccess {String} info.nick_name Nick name.
     * @apiSuccess {String} info.email Email.
     * @apiSuccess {String} info.phone Pho.ne
     * @apiSuccess {String} info.avatar Avatar.

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : {},
}
     *
     * @apiUse APIEditError
     */
    public function editAction()
    {
        if (!empty($this->device->user_id)) {
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'account not exists';
            return $this->send();
        }
        $user = Users::findFirst($this->device->user_id);
        if ($user) {
            $user->first_name = $this->request->getPost("first_name", "striptags", "");
            $user->last_name  = $this->request->getPost("last_name", "striptags", "");
            $user->nick_name  = $this->request->getPost("nick_name", "striptags", "");
            $user->email      = $this->request->getPost("email", "striptags", "");
            $user->phone      = $this->request->getPost("phone", "striptags", "");
            $avatar = $this->uploadAvatar();
            if (!empty($avatar)) {
                $user->avatar  = $avatar;
            }
            if ($user->save())
            {
                $user->refresh();
                $user_info = $user->toArray();
                unset($user_info['create_datetime']);
                unset($user_info['approved']);

                $this->_response['status']  = ResponseStatus::SUCCESS;
                $this->_response['message'] = 'success';
                $this->_response['info'] = $user_info;
            }
        }
        return $this->send();
    }

        /**
     * @apiDescription Get device profile
     *
     * @api {post} /api/profile/info Info
     * @apiName ProfileInfo
     * @apiGroup API-Profile
     * @apiVersion 0.0.1
     *
     * @apiSuccess {Integer} status Status Code.
     * @apiSuccess {String} message Status

     * @apiSuccess {Object} info Owner Info
     * @apiSuccess {Number} info.id Id.
     * @apiSuccess {String} info.first_name First name.
     * @apiSuccess {String} info.last_name Last name.
     * @apiSuccess {String} info.nick_name Nick name.
     * @apiSuccess {String} info.email Email.
     * @apiSuccess {String} info.phone Pho.ne
     * @apiSuccess {String} info.avatar Avatar.

     * @apiSuccess {String} [tkey] CSRF token name
     * @apiSuccess {String} [tval] CSRF token value
     *
     * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "info" : {},
}
     *
     * @apiUse APIGlobalError
     */
    public function infoAction()
    {
        if (!empty($this->device->user_id)) {
            $this->_response['message'] = 'error';
            $this->_response['errors'][] = 'account not exists';
            return $this->send();
        }
        $user = Users::findFirst($this->device->user_id);
        if ($user) {
            $user_info = $user->toArray();
            unset($user_info['create_datetime']);
            unset($user_info['approved']);

            $this->_response['status']  = ResponseStatus::SUCCESS;
            $this->_response['message'] = 'success';
            $this->_response['info'] = $user_info;
        } else {
            $this->_response['message'] = 'error';
        $this->_response['errors'][]   = 'Profile not found';
        }
        return $this->send();
    }

    public function listAction()
    {
        $this->_response['message'] = 'error';
        $this->_response['errors'][]   = 'Invalid action';
        return $this->send()
    }

    public function deleteAction()
    {
        $this->_response['message'] = 'error';
        $this->_response['errors'][]   = 'Invalid action';
        return $this->send()
    }

    private function uploadAvatar()
    {
        $image_url ='';

        $temp_dir = $this->config->avatar->temp_path;
        $dest_dir = $this->config->avatar->upload_path;
        $dest_url = $this->config->avatar->upload_url;

        //Print the real file names and their sizes
        $this->logger->log(LOG_INFO,"POINT:: Find image...");
        foreach ($this->request->getUploadedFiles() as $file)
        {
            $file_name = $file->getName();
            $file_size = $file->getSize();
            $this->logger->log(LOG_INFO,"POINT:: Name[".$file_name."] Size[". $file_size."]");
            if (empty($file_name) || ($file_size == 0)) {
                continue;
            }

            $tmp_name = Utils::generateRandom(32);
            $tpath = $temp_dir.$tmp_name.'.jpg';
            $opath = $dest_dir.$tmp_name.'.jpg';

            $file->moveTo($tpath);
            if (file_exists($tpath))
            {
                $image = new Image($tpath);
                //Fix image orientation
                $image->fixOrientation();
                if ($image->normalizeSize(600, 600) && $image->save($opath))
                {
                  unlink($tpath);
                  $image_url = $this->basehref.$dest_url.$tmp_name.'.jpg';
                }
            }
        }
        return $image_url;
    }
}
