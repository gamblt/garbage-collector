<?php
/**
 * User Roles Enum
 *
 * @package UserRole
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Enums;
/***/
class UserRole extends \Garbage\Enums\Base\Enum
{
  const GUEST       = 0;
  const USER        = 1;
  const VERIFIED    = 2;
  const PUBLISHER   = 3;
  const MODERATOR   = 4;
  const ADMIN       = 5;  //Admin in subdomain scope
  const SUPERADMIN  = 10;

  /**
   * @copydoc
   */
  public static function getStrings()
  {
      return [
          self::GUEST     => 'Гость',
          self::USER      => 'Пользователь',
          self::VERIFIED  => 'Проверенный',
          self::PUBLISHER => 'Автор',
          self::MODERATOR => 'Модератор',
          self::ADMIN     => 'Администратор',
          self::SUPERADMIN=> 'Суперадминистратор',
      ];
  }
}
