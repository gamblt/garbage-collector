<?php
/**
 * Config
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 * @author Aleksandr Shkarbaliuk (Gambit)
 */

namespace Garbage\Config;

class Config
{
    /**
     * Get config scoped in city subdomain
     * @param string $city
     * @return type
     */
    public static function get($city) {

        $config = new \Phalcon\Config(require(__DIR__.'/base.php'));
        if (file_exists(__DIR__.'/local.php')) {
            $local = new \Phalcon\Config(require(__DIR__.'/local.php'));
            $config->merge($local);
        }

        //Merge with city config
        if (file_exists(__DIR__.'/cities/'.$city.'.php')) {
            $city_config = new \Phalcon\Config(require(__DIR__.'/cities/'.$city.'.php'));
            $config->merge($city_config);
        }
        else
        {
            //echo "file [".__DIR__.'/cities/'.$city.'.php'."] not exists!"; exit();
        }
        return $config;
    }
}