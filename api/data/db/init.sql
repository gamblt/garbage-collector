--Create postgres cluster

sudo pg_createcluster --locale en_US.UTF-8 --start 9.6 garbage
sudo su postgres
createuser -p 5440 -P -s -l gambit
createuser -p 5440 -P -l garbage     --Q4ws7d6t9BsPPBLGyCgfsr5U




--PostGis init
/** @see https://gis.stackexchange.com/questions/71302/error-when-trying-to-run-create-extension-postgis */
sudo apt-get install postgis postgresql-9.6-postgis-scripts

/** @see http://postgis.net/install/ */
-- Enable PostGIS (includes raster)
CREATE EXTENSION postgis;
-- Enable Topology
--CREATE EXTENSION postgis_topology;
-- Enable PostGIS Advanced 3D
-- and other geoprocessing algorithms
-- sfcgal not available with all distributions
--CREATE EXTENSION postgis_sfcgal;
-- fuzzy matching needed for Tiger
--CREATE EXTENSION fuzzystrmatch;
-- rule based standardizer
--CREATE EXTENSION address_standardizer;
-- example rule data set
--CREATE EXTENSION address_standardizer_data_us;
-- Enable US Tiger Geocoder
--CREATE EXTENSION postgis_tiger_geocoder;
