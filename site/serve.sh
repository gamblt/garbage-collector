#!/bin/bash

rm -rf ./dist/* -R
rm -rf ./tmp/* -R
ng serve --ssl --disableHostCheck
#ng serve --prod --aot