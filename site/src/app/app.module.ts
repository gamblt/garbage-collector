import { NgModule, Injectable, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { HeaderComponent } from './header/header.component';
import { AddPointComponent } from './add-point/add-point.component';
import { ProfileComponent } from './profile/profile.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { LocationService, PointService } from './services';

import { routing } from './app.routing';
import { CustomHttpProvider } from './custom.http.provider';


@Injectable()
export class AppErrorHandler implements ErrorHandler {

  constructor(
    //private _logService: LogService
  ) {}

  handleError(error) {
    console.warn('ANGULAR CUSTOM LOGGER [%o]', error);
    //let error_msg = error.originalError || 'Server error';
    //this._logService.log('GlobalError', error_msg, error);
  }
}


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    MapComponent,
    AddPointComponent,
    NotFoundComponent,
    HeaderComponent,
    ProfileComponent
  ],
  providers: [LocationService, PointService],
  bootstrap: [AppComponent]
})
export class AppModule { }
