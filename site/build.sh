#!/bin/bash

rm -rf ./dist/* -R
rm -rf ./tmp/* -R
ng build --prod --aot