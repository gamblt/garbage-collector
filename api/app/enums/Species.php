<?php
/**
 * Species Enum
 *
 * @package Species
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2017 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Enums;
/***/
class Species extends \Garbage\Enums\Base\Enum
{
  const UNDEFINED = 0;
  const ORGANIC   = 1;
  const PLASTIC   = 2;
  const GLASS     = 3;
  const PAPER     = 4;
  const BATTERY   = 5;

  /**
   * @copydoc
   */
  public static function getStrings()
  {
    return [
      self::UNDEFINED => 'Неизвестно',
      self::ORGANIC   => 'Органика',
      self::PLASTIC   => 'Пластик',
      self::GLASS     => 'Стекло',
      self::PAPER     => 'Бумага',
      self::BATTERY   => 'Батарейки'
      /**
       * @todo Подумать как разделять мусорки и пункты по приему бумаги/пластика/батареек и прочего добра...
      */
    ];
  }
}
