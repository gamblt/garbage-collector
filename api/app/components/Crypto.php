<?php
/**
 * @package Crypt;
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

/***/
class Crypto extends Component
{
    /**
     * Secret Key for hash functions
     */
    private $_hash_secret_key = 'DefaultSecretKey';

    /**
     * Crypto constructor
     * @return type
     */
    function __construct() {
        $config = $this->di->get('config')->security;
        $this->_hash_secret_key = $config->hash_secret_key;
    }

    /**
     * Create URL signature
     * @param type $url
     * @return type
     */
    public function createUrlSignature($url) {
        return hash_hmac('sha256', $url, $this->_hash_secret_key, false);
    }

    /**
     * Check URL signature
     * @param type $url
     * @param type $signature
     * @return type
     */
    public function checkUrlSignature($url, $signature) {
        $calc_signature = $this->createUrlSignature($url);
        return (!strcmp($calc_signature, $signature));
    }

}