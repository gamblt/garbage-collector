<?php

/**
 * Imagick wrapper
 * Edit Images functions
 *
 * @package Image;
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

/***/
class Image extends Component
{
    /**
     * Imagick instance
     * @var \Imagick
     */
    private $_imagick;


    /**
     * Imagick file path
     */
    private $_file_path = '';


    /**
     * Imagick filter
     * @var int
     */
    private $_filter = \Imagick::FILTER_CATROM;


    /**
     * Create Imagick instance from file
     *
     * @param type $file Full file path
     *
     * @return Image
     */
    function __construct($file)
    {
        $this->_file_path = $file;
        $this->_imagick = new \Imagick($file);
        $this->_imagick->setImageFormat( "jpg" );
        $this->_imagick->setImageBackgroundColor('white');
        $this->_imagick = $this->_imagick->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN);

        //$this->_imagick->setImageCompression(\Imagick::COMPRESSION_LZW);
        $this->_imagick->setCompressionQuality(90);
        $this->_imagick->stripImage();
    }

    /**
     * Pass base Imagick methods
     *
     * @param string $method Imagick method name
     * @param array $args Imagick method args
     *
     * @return any
     */
    public function __call($method, $args)
    {
        if(is_callable(array($this->_imagick, $method))) {
            return call_user_func_array(array($this->_imagick, $method), $args);
        } else {
            throw new Exception("No Valid Image method", 1);

        }
    }

    /**
     * Resize image
     *
     * @param int $width New image width
     * @param int $height New image height
     *
     * @return boolean
     */
    public function resize($width, $height) {
        return $this->_imagick->resizeImage($width, $height, $this->_filter, 1);
    }


    /**
     * Crop image
     *
     * @param int $w Crop area width
     * @param int $h Crop area height
     * @param int $x Crop area X offset
     * @param int $y Crop area Y offset
     *
     * @return bool
     */
    public function crop($w, $h, $x, $y) {
        return $this->_imagick->cropImage($w, $h, $x, $y);
    }


    /**
     * Get image size
     *
     * @return array
     */
    public function getSize()
    {
        return $this->_imagick->getImageGeometry();
    }


    /**
     * Get image orientation
     *
     * @return string
     */
    public function getOrientation()
    {
        $geometry = $this->_imagick->getImageGeometry();
        if ($geometry['width'] > $geometry['height']) {
            return 'landscape';
        } else {
            return 'portrait';
        }
    }

    /**
     *
     * Auto Resize image if it's large
     *
     * @param int $maxWidth New image max width
     * @param int $maxHeight New image max height
     *
     * @return boolean
     */
    public function normalizeSize($maxWidth, $maxHeight)
    {
        $geometry = $this->_imagick->getImageGeometry();
        $orientation = $this->getOrientation();
        if ($orientation == 'landscape' && $geometry['width'] > $maxWidth)
        {
            $w = $maxWidth;
            $h = $maxWidth * $geometry['height'] / $geometry['width'];
            return $this->_imagick->resizeImage($w, $h, $this->_filter, 1);
        }
        elseif ($orientation == 'portrait' && $geometry['height'] > $maxHeight)
        {
            $w = $maxHeight * $geometry['width'] / $geometry['height'];
            $h = $maxHeight;
            return $this->_imagick->resizeImage($w, $h, $this->_filter, 1);
        }
        return true;
    }

    /**
     * Save image
     *
     * @param string $dest Save path
     *
     * @return bool
     */
    public function save($dest)
    {
        return $this->_imagick->writeImage($dest);
    }


    public function fixOrientation() 
    {
        try {
            $exif = @exif_read_data($this->_file_path);
       
            $this->logger->log(LOG_INFO, "Image EXIF:
                [".print_r($exif, true)."]
            ");

            $orientation = 0;
            if( isset($exif['Orientation']) )
                $orientation = $exif['Orientation'];
            elseif( isset($exif['IFD0']['Orientation']) )
                $orientation = $exif['IFD0']['Orientation'];
            else {
                return false;
            }

            switch($orientation) 
            {
                case \Imagick::ORIENTATION_BOTTOMRIGHT: // rotate 180 degrees
                    $this->_imagick->rotateimage("#FFF", 180);
                break;

                case \Imagick::ORIENTATION_RIGHTTOP: // rotate 90 degrees CW
                    $this->_imagick->rotateimage("#FFF", 90);
                break;

                case \Imagick::ORIENTATION_LEFTBOTTOM: // rotate 90 degrees CCW
                    $this->_imagick->rotateimage("#FFF", -90);
                break;
            }
            $this->_imagick->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
        } catch (Exception $e) {
            $this->logger->log(LOG_ERR,"Read Exif Error [".$e->getMessage()."]");
        }
    }

}
