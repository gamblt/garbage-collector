<?php
/**
 * Mailer component.
 * Send internal and external mails
 *
 * @package Mailer;
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

use \Garbage\Shared\Models\Users;
use \Garbage\Shared\Models\Mails;

// Send mail throw external SMTP server
require_once(__DIR__.'/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php');

// Send mail throw local postfix server
//require_once(__DIR__.'/../../vendor/phpmailer/class.phpmailer.php');
//require_once(__DIR__.'/../../vendor/phpmailer/class.smtp.php');

/***/
class Mailer extends Component
{
    /**
     * Mail locale
     * @var string
     */
    private $_default_locale = 'ru';


    /**
     * Send Internal Email throw External SMTP server
     *
     * @param string $to Mail recepient
     * @param string $subject Mail subject
     * @param string $body Mail HTML body
     * @param string $plaintext Mail plain body
     * @param string $replyto Sender reply-to email
     *
     * @return boolean
     */
    public function sendInternal($to, $name, $subject, $body, $plaintext = '', $replyto = '')
    {
        //Get mailer config
        $config = $this->di->get('config')->mailer;

        // Create a message
        $plain = ($plaintext) ? $plaintext : "Plain text does not support";
        if (empty($replyto)) {
            $replyto = $config->from;
        }
        $message = \Swift_Message::newInstance()
            ->setFrom(array($config->from => $config->from_name))
            ->setTo(array($to => $name))
            ->setContentType('text/html')
            ->setSubject($subject)
            ->setBody($body)
            ->setReplyTo($replyto)
            ->addPart($plain, 'text/plain');

        // Create transport;
        $transport = \Swift_SmtpTransport::newInstance($config->server, $config->port, $config->secure)
            ->setUsername($config->login)
            ->setPassword($config->password);
        $mailer = \Swift_Mailer::newInstance($transport);

        // Send the message
        return $mailer->send($message);
    }

    /**
     * Send external mail template to user
     * (Send Request to mail server)
     *
     * @param string $to Mail recepient
     * @param type $name Recepient name
     * @param type $mail_id Mail template ID
     * @param type $locale Mail locale
     * @param array $data Mail custome field
     * @param boolean $send_once SendOnce mode
     *
     * @return array Response info
     */
    public function send($to, $name, $mail_id, $locale = 'ru', $data = [], $send_once = false)
    {
        $userid = $this->session->id;
        /*$this->activity->add($userid, 'SendTemplate', 'Init', null, $to, $mail_id, $locale, $data);*/

        $user = Users::findFirst([
            "conditions" => "email = ?1",
            "bind"       => [1 => $to]
        ]);
        if ($user) {
            $user_id = $user->id;
        } else {
            $user_id = 0;
        }

        $config = $this->di->get('config')->api;

        $bridge = new APIBridge();
        $data = $bridge->buildRequest(
            $config->appid,
            $config->apikey, [
                'user_id'   => $user_id,
                'name'      => $name,
                'email'     => $to,
                'mail_id'   => $mail_id,
                'locale'    => $locale,
                'send_once' => $send_once,
                'variables' => $data
            ]
        );

        $curl = new CurlSender();
        $resp = $curl->sendRequest('POST', $config->endpoints->mailer.'/send/', $data);

        $this->logger->log(LOG_INFO,'SendTemplate Curl Response ['.print_r($resp, true).']');

        if (isset($resp['content'])) {
            return json_decode($resp['content'], true);
        } else {
            return $resp;
        }
    }
}
