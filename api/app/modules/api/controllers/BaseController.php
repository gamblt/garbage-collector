<?php
/**
 * Base API Controller
 *
 * @package APIBaseController
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */
namespace Garbage\API\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
//use \Garbage\Plugins\Security;

use Garbage\Shared\Models\Devices;
use Garbage\Shared\Models\Requests;
use Garbage\Enums\ResponseStatus;

/***/
class BaseController extends Controller
{
    protected $device; //Devices object

    // ACL Security trait
   //use \Garbage\Traits\Security;


    /**
     * API response fields
     * @var array
     */
    protected $_response = [];


    /**
     * Init controller
     * Get fields from php-input (Angular ajax feature)
     * Set default _response fields
     */
    public function initialize()
    {
        //Angular request support Hack!
        $params = json_decode(file_get_contents('php://input'), true);
        $this->logger->log(LOG_INFO,'BASE POST ['.print_r($_POST, true).']');
        $this->logger->log(LOG_INFO,'BASE PARAMS ['.print_r($params, true).']');
        $this->logger->log(LOG_INFO,'BASE FILES ['.print_r($_FILES , true).']');
        if (is_array($params)) {
            $_POST = array_merge($params, $_POST);
        }

        $device_id    = $this->request->getPost("device_id", "striptags", "undefined");
        $device_name  = $this->request->getPost("device_name", "striptags", "undefined");
        $this->device = $this->getDevice($device_id, $device_name);

        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";

        $request = new Requests();
        $request->ip           = $ip;
        $request->useragent    = $ua;
        $request->useragent_hash=md5($ua);
        $request->url          = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $request->device_id    = $this->device->id;
        $request->referer      = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
        $request->get          = json_encode($_GET);
        $request->post         = json_encode($_POST);
        $request->save();



        /**
         * @apiDefine APIGlobalSuccess
         * @apiSuccess {Integer} status Status Code.
         * @apiSuccess {String} message Status
         * @apiSuccess {String} tkey CSRF token name
         * @apiSuccess {String} tval CSRF token value
         * @apiSuccessExample {json} Success-Response:
{
    "status" : 200,
    "message" : "success",
    "tkey":'xxx',
    "tval":'yyy'
    }
}
        */


        /**
         * @apiDefine APIGlobalError
         * @apiError {Integer} status Status Code.
         * @apiError {String} message Status
         * @apiError {String} tkey CSRF token name
         * @apiError {String} tval CSRF token value
         * @apiErrorExample {json} Error-Response:
{
    "status" : 500,
    "message" : "error",
    "tkey":'xxx',
    "tval":'yyy'
    }
}
        */
        // Init response
        $this->_response = [
            "status"=> ResponseStatus::GENERAL_ERROR,
            "message"=> "error",
            "tkey" => $this->security->getTokenKey(),   // new csrf token name
            "tval" => $this->security->getToken()       // new csrf token value
        ];
    }

    /**
     * Send response to user.
     * @return Response
     */
    protected function send($is_rest = false)
    {
        if ($is_rest) {
            unset($this->_response['tkey']);
            unset($this->_response['tval']);
        }
        //$this->logger->log(LOG_INFO,'Return DATA['.print_r($this->_response, true).']');
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($this->_response));
        return $this->response;
    }

    protected function getDevice($device_id, $device_name)
    {
        $device = Devices::findFirst([
            "conditions" => "device_id = :did:",
            "bind" => ["did" => $device_id]
        ]);
        if (!$device) {
            $device = new Devices();
            $device->device_id = $device_id;
            $device->device_name = $device_name;
            $device->save();
        }
        return $device;
    }
}
