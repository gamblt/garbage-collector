import { GarbageCollectorPage } from './app.po';

describe('garbage-collector App', () => {
  let page: GarbageCollectorPage;

  beforeEach(() => {
    page = new GarbageCollectorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
