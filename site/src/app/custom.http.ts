import { Injectable } from '@angular/core';
import { Http, Headers, Request,
  RequestOptions, RequestOptionsArgs,
  ConnectionBackend, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';


declare  var $:any;

@Injectable()
export class CustomHttp extends Http {

  private _token: any;

  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions)
  {
    super(backend, defaultOptions);
    this._token = $('#atkn');
  }

  addOptions(options?: RequestOptionsArgs): RequestOptionsArgs
  {
    //console.log('Add OPTIONS...');
    if (!options) {
      options = {};
    }
    //Get CSRF token/value from HTML
    let headers: any = {
      'Content-Type': 'application/json',
      'X-Requested-With':'XMLHttpRequest'
    }
    //console.log('REQUEST TOKEN [%o][%o]', this._token, this._token[0]);
    if (typeof this._token !== 'undefined') {
      if (this._token.attr('name') && this._token.val()){
        let tkey = 'x-sess-tkn-' + this._token.attr('name');
        let tval = this._token.val();
        //console.error('TKEY[%o] TVAL[%o]', tkey, tval);
        headers[tkey] = tval;
      } else {
        //console.error('No valid attributers in token[%o]', this._token);
      }
      //console.log('REQUEST HEADERS [%o]', headers);
    } else {
      //console.error('A-TOKEN NOT FOUND!!!');
    }
    options.headers = new Headers(headers);
    return options;
  }

  updateToken(resp)
  {
    if ((typeof resp.tkey !== 'undefined') && (typeof resp.tval !== 'undefined'))
    {
      if (this._token) {
        //console.log('UPDATE A-TOKEN [%s][%s][%o]', resp.tkey, resp.tval, this._token);
        this._token.attr('name', resp.tkey);
        this._token.val(resp.tval);
      }
    }
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response>
  {
    //console.log('CUSTOM REQUEST... URL[%o] OPTS[%o]', url, options);
    return super.request(url, this.addOptions(options))
    .do((data) => {
      let resp = data.json()
      //console.log('Post response data-json:[%o]', resp);
      this.updateToken(resp);
      if (resp.status == 301) {
        //NEED_LOG_IN
        //console.warn('Need login');
      } else if (resp.status == 302) {
        //ACCESS_DENIED
        //console.warn('Access denied');
      }
    })
    .catch(res => {
      //console.error('Custom HTTP catcher: [%o]', res);
      return Observable.throw(res);
    });
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response>
  {
    //console.log('CUSTOM GET... URL[%o] OPTS[%o]', url, options);
    return super.get(url, this.addOptions(options))
    .do((data) => {
      let resp = data.json()
      this.updateToken(resp);
      if (resp.status == 301) {
        //NEED_LOG_IN
        //console.warn('Need login');
      } else if (resp.status == 302) {
        //ACCESS_DENIED
        //console.warn('Access denied');
      }
    })
    .catch(res => {
      //console.error('Custom HTTP catcher: [%o]', res);
      return Observable.throw(res);
    });
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>
  {
    //console.log('CUSTOM POST...URL[%o] BODY[%o] OPTS[%o]', url, body, options);
    return super.post(url, body, this.addOptions(options))
    .do((data) => {
      let resp = data.json()
      //console.log('Post response data-json:[%o]', resp);
      this.updateToken(resp);
      if (resp.status == 301) {
        //NEED_LOG_IN
        //console.warn('Need login');
      } else if (resp.status == 302) {
        //ACCESS_DENIED
        //console.warn('Access denied');
      }
    })
    .catch(res => {
      //console.error('Custom HTTP catcher: [%o]', res);
      return Observable.throw(res);
    });
  }
}
