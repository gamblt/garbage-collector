import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/observable/throw';

declare var $: any;
declare var sha256:any;

@Injectable()
export class BaseService
{
  constructor() {}

  protected handleError(error: any)
  {
    //Hide loader if error
    $('#loader').hide();
    console.error('BaseServiceErrorHandler ERR[%o]', error);
    let error_msg = error.originalError || 'Server error';
    this.sendError('ServiceError', error_msg, error);
    return Observable.throw(error_msg);
  }


  protected sendError(type: string, message: string, data: any)
  {
    console.error('Send Error! [%s]', message);
    let nav = this.objectSerializer(navigator);
    let scr = this.objectSerializer(window.screen);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/log/handler', true);
    xhr.send(JSON.stringify({
      'type'      : type,
      'message'  : message,
      'data'     : JSON.stringify(data),
      'screen'  : scr,
      'navigator'  : nav,
      'url'       : window.location.href
    }));
  }

  protected hash(params: any)
  {
    if (typeof(params) == 'undefined') {
      params = {};
    }
    let str = this.objectSerializer(params, true);
    if (str !== 'false') {
      let hash = sha256(str);
      console.log('Params hash[%o]', hash);
      return hash;
    } else {
      return '';
    }
  }

  /**
   * Object serializer for 'navigator' variable
   */
  protected objectSerializer(object: any, boolreturn = false)
  {
    let type = typeof object;
    if (object === null) {
      if (boolreturn) return 'false';
      return '"null"';
    }
    if (type == 'string' || type === 'number' || type === 'boolean') {
      return '"' + object + '"';
    }
    else if (type === 'function') {
      if (boolreturn) return 'false';
      return '"functionValue"';
    }
    else if (type === 'object') {
      let output = '{';
      for (let item in object) {
        try {
          if (
            (item !== 'enabledPlugin')
            && (item !== 'mimeTypes')
            && (item !== 'plugins')
            ) {
            let val = this.objectSerializer(object[item]);
            if (val !== '"functionValue"') {
              output += '"' + item + '":' + val + ',';
            }
          }
        }
        catch (e) {
        }
      }
      return output.replace(/\,$/, '') + '}';
    }
    else if (type === 'undefined') {
      if (boolreturn) return 'false';
      return '"undefinedError"';
    }
    else {
      if (boolreturn) return 'false';
      return '"unknownTypeError"';
    }
  };
}
