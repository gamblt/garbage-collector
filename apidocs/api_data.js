define({ "api": [
  {
    "description": "<p>Add new point</p> ",
    "type": "post",
    "url": "/api/point/add",
    "title": "Add",
    "name": "PointAdd",
    "group": "API_Point",
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lat",
            "description": "<p>Point latitude</p> "
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lng",
            "description": "<p>Point longitude</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "0",
              "1",
              "2",
              "3",
              "4"
            ],
            "optional": true,
            "field": "type",
            "defaultValue": "0",
            "description": "<p>Point type</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Point name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Point description</p> "
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": true,
            "field": "image",
            "description": "<p>Point image</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "point",
            "description": "<p>Point Info - (DEPRECATED!!! Use info field)</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "info",
            "description": "<p>Point Info</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.id",
            "description": "<p>Id.</p> "
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "info.lat",
            "description": "<p>Latitude</p> "
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "info.lng",
            "description": "<p>Longitude</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.type",
            "description": "<p>Type.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.name",
            "description": "<p>Name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.description",
            "description": "<p>Description.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.image",
            "description": "<p>Image.</p> "
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "info.create_datetime",
            "description": "<p>Create datetime timestamp.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"info\" : {},\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/PointController.php",
    "groupTitle": "API_Point",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors list</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"errors\": [\"Item save error\"],\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Edit exists point if device is owner of this point</p> ",
    "type": "post",
    "url": "/api/point/edit",
    "title": "Edit",
    "name": "PointEdit",
    "group": "API_Point",
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Point ID</p> "
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "lat",
            "description": "<p>Point latitude</p> "
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "lng",
            "description": "<p>Point longitude</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "0",
              "1",
              "2",
              "3",
              "4",
              "5"
            ],
            "optional": true,
            "field": "type",
            "description": "<p>Point type</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Point name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Point description</p> "
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": true,
            "field": "image",
            "description": "<p>Point image</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "info",
            "description": "<p>Point Info</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.id",
            "description": "<p>Id.</p> "
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "info.lat",
            "description": "<p>Latitude</p> "
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "info.lng",
            "description": "<p>Longitude</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.type",
            "description": "<p>Type.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.name",
            "description": "<p>Name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.description",
            "description": "<p>Description.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.image",
            "description": "<p>Image.</p> "
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "info.create_datetime",
            "description": "<p>Create datetime timestamp.</p> "
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "info.species",
            "description": "<p>Species[] species.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"info\" : {},\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/PointController.php",
    "groupTitle": "API_Point",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors list</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"errors\": [\"Edit model field error\"],\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Get nearby points if lat/lgn passed.<br/> Return 10 last points if lat/lng not passed.</br> Return points with specified type if some &quot;mode&quot; passed...(not implemented)<br/></p> ",
    "type": "post",
    "url": "/api/point/list",
    "title": "List",
    "name": "PointList",
    "group": "API_Point",
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "lat",
            "description": "<p>User latitude</p> "
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": true,
            "field": "lng",
            "description": "<p>User longitude</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "mode",
            "description": "<p>List mode</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "points",
            "description": "<p>Point[] Info - (DEPRECATED!!! Use list field)</p> "
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": "<p>Points array (See PointInfo object structure)</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"info\" : {},\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/PointController.php",
    "groupTitle": "API_Point",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Get points species</p> ",
    "type": "post",
    "url": "/api/point/species",
    "title": "Species",
    "name": "SpeciesList",
    "group": "API_Point",
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "type",
            "description": "<p>Species type</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": "<p>Species array: String[]</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "type",
            "description": "<p>Species: - Species[] list one item!</p> "
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "type.id",
            "description": "<p>ID</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type.name",
            "description": "<p>Name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type.icon",
            "description": "<p>Icon</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"list\" : [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/PointController.php",
    "groupTitle": "API_Point",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Get points types</p> ",
    "type": "post",
    "url": "/api/point/types",
    "title": "Types",
    "name": "TypesList",
    "group": "API_Point",
    "version": "0.0.1",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": "<p>Types array: String[]</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "type",
            "description": "<p>Type: - Types[] list one item!</p> "
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "type.id",
            "description": "<p>ID</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type.name",
            "description": "<p>Name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type.icon",
            "description": "<p>Icon</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"list\" : [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/PointController.php",
    "groupTitle": "API_Point",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Add new profile for device</p> ",
    "type": "post",
    "url": "/api/profile/add",
    "title": "Add",
    "name": "ProfileAdd",
    "group": "API_Profile",
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "first_name",
            "description": "<p>Owner first name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "last_name",
            "description": "<p>Owner last name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "nick_name",
            "description": "<p>Owner nick name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": "<p>Owner email</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phone",
            "description": "<p>Owner phone</p> "
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": true,
            "field": "avatar",
            "description": "<p>Owner avatar</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "info",
            "description": "<p>Owner Info</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.id",
            "description": "<p>Id.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.first_name",
            "description": "<p>First name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.last_name",
            "description": "<p>Last name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.nick_name",
            "description": "<p>Nick name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.email",
            "description": "<p>Email.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.phone",
            "description": "<p>Pho.ne</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.avatar",
            "description": "<p>Avatar.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"info\" : {},\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/ProfileController.php",
    "groupTitle": "API_Profile",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors list</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"errors\": [\"Item save error\"],\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Edit profile for device</p> ",
    "type": "post",
    "url": "/api/profile/edit",
    "title": "Edit",
    "name": "ProfileEdit",
    "group": "API_Profile",
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "first_name",
            "description": "<p>Owner first name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "last_name",
            "description": "<p>Owner last name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "nick_name",
            "description": "<p>Owner nick name</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": "<p>Owner email</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phone",
            "description": "<p>Owner phone</p> "
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": true,
            "field": "avatar",
            "description": "<p>Owner avatar</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "info",
            "description": "<p>Owner Info</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.id",
            "description": "<p>Id.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.first_name",
            "description": "<p>First name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.last_name",
            "description": "<p>Last name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.nick_name",
            "description": "<p>Nick name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.email",
            "description": "<p>Email.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.phone",
            "description": "<p>Pho.ne</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.avatar",
            "description": "<p>Avatar.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"info\" : {},\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/ProfileController.php",
    "groupTitle": "API_Profile",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors list</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"errors\": [\"Edit model field error\"],\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "description": "<p>Get device profile</p> ",
    "type": "post",
    "url": "/api/profile/info",
    "title": "Info",
    "name": "ProfileInfo",
    "group": "API_Profile",
    "version": "0.0.1",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "info",
            "description": "<p>Owner Info</p> "
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "info.id",
            "description": "<p>Id.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.first_name",
            "description": "<p>First name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.last_name",
            "description": "<p>Last name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.nick_name",
            "description": "<p>Nick name.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.email",
            "description": "<p>Email.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.phone",
            "description": "<p>Pho.ne</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "info.avatar",
            "description": "<p>Avatar.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\" : 200,\n    \"message\" : \"success\",\n    \"info\" : {},\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../api/app/modules/api/controllers/ProfileController.php",
    "groupTitle": "API_Profile",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Status Code.</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Status</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tkey",
            "description": "<p>CSRF token name</p> "
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "tval",
            "description": "<p>CSRF token value</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\" : 500,\n    \"message\" : \"error\",\n    \"tkey\":'xxx',\n    \"tval\":'yyy'\n    }\n}",
          "type": "json"
        }
      ]
    }
  }
] });