<?php
/**
 * Realize Phalcon\Session\Adapter Abstract class.
 * @see https://docs.phalconphp.com/en/latest/api/Phalcon_Session_Adapter.html
 *
 * @package SessionHandler
 *
 * @author Aleksandr Shkarbaliuk (Gambit)
 * @version 1.0.0
 * @copyright Copyright (c) 2016 Aleksandr Shkarbaliuk (Gambit)
 *
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

/***/
class SessionHandler extends Component
{
    /**
     * Session TTL in sec
     * @var int
     */
    protected $_ttl = 10000;

    /**
     * Is session started flag
     * @var bool
     */
    protected $_isstarted = false;

    /**
     * Session ID
     * @var string|false
     */
    private $_sid = false;

    /**
     * Session prefix in Redis
     * @var string
     */
    protected $_prefix = 'sessions';

    /**
     * Session Redis instance
     * @var Redis
     */
    protected $redis = false;

    /**
     * Loagger instance
     * @var Loaager
     */
    protected $logger = false;

    /**
     * Session status Enum
     * @var int[]
     */
    static private $SESSION_ACTIVE = 2;
    static private $SESSION_NONE = 1;
    static private $SESSION_DISABLED = 0;


    /**
     * Create and configure session connection
     * @param array $options Session options
     * @return SessionHandler
     */
    function __construct($options = [])
    {
        $di = \Phalcon\DI::getDefault();
        $this->logger = $di->getShared('logger');

        $redis_config = $di->getShared('config');
        $config = $redis_config->session->redis;
        try {
            $this->redis = new \Redis();
            $this->redis->connect(
                $config->host,
                $config->port,
                $config->timeout
            );
            $this->redis->auth(
                $config->password
            );
            $this->redis->select(
                $config->db
            );
            $this->redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
            $this->redis->setOption(\Redis::OPT_PREFIX, $config->prefix.":sessions:");
        }
        catch (Exception $e) {
            //Use direct sysog to prevent recursive erors by Logger collector
            syslog(LOG_ERR,"Redis Connection error: [".$e->getMessage()."]");
        }

        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Start the session
     * @warning If headers are already sent the session will not be started!
     *
     * @return bool
     */
    public function start()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
        $this->_sid = session_id();
        $this->logger->log(LOG_INFO, "SESSHAND:: Start Session SID detected:[".$this->_sid."]");
        $this->isstarted = true;
        //Create hash
        $this->redis->hSet($this->_prefix.':'.$this->_sid, 'start', '1');
        //Set hash expiration
        $this->redis->expire($this->_prefix.':'.$this->_sid, $this->_ttl);
        return true;
    }


    /**
     * Clear session from redis storage
     * @return bool
     */
    public function clear()
    {
        $this->logger->log(LOG_INFO, "SESSHAND:: Clear session...");
        session_unset();
        $this->redis->delete($this->_prefix.':'.$this->_sid);
        return true;
    }


    /**
     * Update session sid with save data
     * @param string $old_sid Old session ID
     * @param string $new_sid New session ID
     * @return bool
     */
    public function update($old_sid, $new_sid)
    {
        $this->logger->log(LOG_INFO, "SESSHAND:: Update session...");
        $old_session = $this->redis->hGetAll($this->_prefix.':'.$old_sid);
        //$this->logger->log(LOG_INFO, "Old session data [".print_r($old_session, true)."]");

        //Create new session hash
        $this->redis->hMSet($this->_prefix.':'.$new_sid, $old_session);

        $new_session = $this->redis->hGetAll($this->_prefix.':'.$new_sid);
        //$this->logger->log(LOG_INFO, "New session data [".print_r($new_session, true)."]");

        //Delete old session hash
        $this->redis->delete($this->_prefix.':'.$old_sid);
        return true;
    }

    /**
     * Print session stored in redis to logs.
     * @uses In Debug mode
     *
     * @param string $sid Session ID
     */
    public function printSession($sid)
    {
        $session = $this->redis->hGetAll($this->_prefix.':'.$sid);
        //$this->logger->log(LOG_INFO, "Print session data [".$sid."][".print_r($session, true)."]");
    }

    /**
     * Destroy current session in memory and clear it from storage
     * @return bool
     */
    public function destroy()
    {
        $this->isstarted = false;
        //if (session_status() === PHP_SESSION_ACTIVE) {
            session_destroy();
        //}
        $this->redis->delete($this->_prefix.':'.$this->_sid);
        return true;
    }

    /**
     * Set session options
     * @param array $options
     */
    public function setOptions($options = [])
    {
        $this->_ttl = 10000;
    }

    /**
     * Get session options
     * @return type
     */
    //public function getOptions() {}

    /**
     * Set session ID
     * @param string $id Session ID
     * @return type
     */
    public function setId($id)
    {
        $this->logger->log(LOG_INFO,"Set session ID [".$id."]");
        session_id($id);
        $this->_sid = $id;
        //Create new hash And Set new hash expiration
        return $this->redis->hSet($this->_prefix.':'.$this->_sid, 'start', '1')
            && $this->redis->expire($this->_prefix.':'.$this->_sid, $this->_ttl);
    }

    /**
     * Get current session ID
     * @return string
     */
    public function getId()
    {
        return $this->_sid;
    }

    /**
     * Set session name
     * @param string $name
     */
    public function setName($name)
    {
        session_name($name);
    }

    /**
     * Get session name
     * @return string
     */
    public function getName()
    {
        return session_name();
    }

    /**
     * Gets a session variable from an application context
     * @param mixed $index
     * @param mixed $defaultValue
     * @param bool $remove
     * @return mixed
     */
    public function get($index, $defaultValue = '', $remove = false)
    {
        return unserialize($this->redis->hGet($this->_prefix.':'.$this->_sid, $index));
    }

    /**
     * Sets a session variable in an application context
     * @param mixed $index
     * @param mixed $value
     */
    public function set($index, $value)
    {
        $this->logger->log(LOG_INFO, "Sess val set[".$this->_prefix.':'.$this->_sid."][".$index."]");
        $this->redis->hSet($this->_prefix.':'.$this->_sid, $index, serialize($value));
    }

    /**
     * Check whether a session variable is set in an application context
     * @param mixed $index
     * @return bool
     */
    public function has($index)
    {
        return $this->redis->hExists($this->_prefix.':'.$this->_sid, $index);
    }

    /**
     * Removes a session variable from an application context
     * @param mixed $index
     */
    public function remove($index)
    {
        $this->redis->hDel($this->_prefix.':'.$this->_sid, $index);
    }

    /**
     * Check whether the session has been started
     * @return bool
     */
    public function isStarted()
    {
        return $this->_isstarted;
    }

    /**
     * Returns the status of the current session. For PHP 5.3 this function will always return SESSION_NONE
     * @return int
     */
    public function status()
    {
        return self::SESSION_NONE;
    }

    /**
     * Alias: Gets a session variable from an application context
     * @param mixed $index
     * @return mixed
     */
    public function __get($index)
    {
        return $this->get($index);
    }

    /**
     * Alias: Sets a session variable in an application context
     * @param mixed $index
     * @param mixed $value
     * @return mixed
     */
    public function __set($index, $value)
    {
        return $this->set($index, $value);
    }

    /**
     * Alias: Check whether a session variable is set in an application context
     * @param mixed $index
     * @return bool
     */
    public function __isset($index)
    {
        return $this->has($index);
    }

    /**
     * Alias: Removes a session variable from an application context
     * @param mixed $index
     */
    public function __unset($index)
    {
        return $this->remove($index);
    }
}
