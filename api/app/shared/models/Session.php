<?php

namespace Garbage\Shared\Models;

use Phalcon\Mvc\Model;
use \Garbage\Components\SessionHandler;

class Session
{
    private $_session;

    private $_ttl;

    /**
     * Session contructor
     * @return type
     */
    function __construct() {
        $di = \Phalcon\DI::getDefault();
        $sess = $di->get('config')->session;
        $this->_ttl = $sess->ttl;

        $this->_session = $di->get('session');

        // server should keep session data for AT LEAST ttl secs
        ini_set('session.gc_maxlifetime', $this->_ttl);
        // each client should remember their session id for EXACTLY ttl secs
        session_set_cookie_params($this->_ttl, '/', '.gladpet.org', $sess->secure, true);
    }

    /**
     * Start session
     * @param type $profile
     * @param type $extradata
     * @return type
     */

    public function startSession()
    {
        $di = \Phalcon\DI::getDefault();
        $config = $di->getShared('config');
        $logger = $di->getShared('logger');

        session_name($config->session->sess);

        $this->_session = new SessionHandler(['ttl' => $this->_ttl]);
        $this->_session->start();

        $logger->log(LOG_INFO,"New session ID [".$this->_session->getId()."]");

        $di->setShared('session', $this->_session);
        return $this->_session;
    }

    public function getId()
    {
      return $this->_session->getId();
    }

    public function dropSession()
    {
        if ($this->_session) {
            $this->_session->destroy();
        }
        session_unset();
    }

    /**
     * Update session id without data drop.
     * @return type
     */
    public function updateSession($sid = false)
    {
      $di = \Phalcon\DI::getDefault();
      $logger = $di->getShared('logger');
      $logger->log(LOG_INFO, "Update session...");
        $old_sid = session_id();
        if (!$sid) {
            //Create new session id
            session_regenerate_id(false);
            $sid = session_id();
        }
        //Copy redis data to new hash
        $this->_session->update($old_sid, $sid);
        $this->_session->setId($sid);
        return $sid;
    }

    public function printSession($sid)
    {
        $this->_session->printSession($sid);
    }

    /**
     * Drop old session and start new
     * @return type
     */
    public function newSession()
    {
        /**
         * Reload session to revent session fixation issue!!!
         * @see https://www.owasp.org/index.php/Session_fixation
         */
        $di = \Phalcon\DI::getDefault();
        //$config = $di->getShared('config');
        $logger = $di->getShared('logger');

        $logger->log(LOG_INFO, "Old SessionID [".session_id()."]");

        if (!$this->_session) {
            $this->_session = new SessionHandler(['ttl' => $this->_ttl]);
            $this->_session->start();
        }

        ///Clear session variables mem+redis
        $this->_session->clear();

        //Update session ID
        if (session_regenerate_id(true)) {
            $logger->log(LOG_INFO, "Session id regenerated.");
        } else {
            $logger->log(LOG_INFO, "Session regenerate id failed!");
        }
        //Set id for redis
        $this->_session->setId(session_id());

        $logger->log(LOG_INFO, "New SessionID [".session_id()."]");

        $di->setShared('session', $this->_session);

        return true;
    }

    /**
     * Description
     * @param type $user
     * @param type $extradata
     * @return type
     */
    public function saveProfile($profile, $extradata = [])
    {
        // Fill session

        // Save Profile fields in session
        foreach ($profile as $key => $value) {
           $this->_session->set($key, $value);
        }
        // Save extrafields in session
        foreach ($extradata as $key => $value) {
           $this->_session->set($key, $value);
        }
        return true;
    }
}
