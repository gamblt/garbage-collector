import { CustomHttp } from './custom.http';
import { Http, RequestOptions, XHRBackend } from '@angular/http';

/**
 * @see https://github.com/angular/angular/issues/11262
 */
export function CustomHttpFactory(backend: XHRBackend, defaultOptions: RequestOptions) {
  return  new CustomHttp(backend, defaultOptions);
}

export let CustomHttpProvider = {
  provide: Http,
  useFactory: CustomHttpFactory,
  deps: [XHRBackend, RequestOptions]
};
