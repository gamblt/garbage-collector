-- Table: public.devices

DROP TABLE IF EXISTS public.devices;

CREATE TABLE public.devices
(
  id bigserial,
  user_id integer,
  device_id character varying,
  device_name character varying,
  create_datetime timestamp with time zone DEFAULT now(),
  CONSTRAINT devices_id_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.devices
  OWNER TO garbage;
