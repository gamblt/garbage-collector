<?php
/**
 * @package Cookies;
 */

namespace Garbage\Components;

use Phalcon\Mvc\User\Component;

use Garbage\Components\Utils;

/***/
class Cookies extends Component
{
    /**
     * @var ttl - cookie lifetime (7 days - 604800) (1 day - 86400)
     */
    protected $_ttl = 86400;
    /**
     * Set test cookie to check if cookies enabled
     */
    public function setTestCookie()
    {
        if (!isset($_COOKIE['tc']) || ($_COOKIE['tc'] != 'set')) {
            setcookie("tc","set", 0, '/', '', false, true);
        }
    }

    /**
     * Check if cookies enabled
     * (When page loaded - test cookie has been set. After that, logo image was loaded and check cookie)
     * @see \Garbage\Site\Controllers\IndexController->logoAction();
     * @return boolean
     */
    public function isCookiesEnabled()
    {
        if (!isset($_COOKIE['tc'])) {
            return false;
        }
        return ($_COOKIE['tc'] == 'set');
    }

    /**
     * Return Cookies lifetime
     * @return type
     */
    public function getTTL()
    {
        return $this->_ttl;
    }

    /**
     * Set unique cookie
     * @param string $path - cookie path
     * @param int $ttl  - cookie ttl
     * @param string &$status - create cookie status
     * @return string = cookie
     */
    public function setUniqueCookie($path, $ttl = false, &$status = '')
    {
        $cookie = '';
        $cookie_name = "uni-".sha1($path);
        //$this->logger->log(LOG_INFO,"URI[".$this->request->getURI()."]");
        //$this->logger->log(LOG_INFO,"COOKIES[".print_r($_COOKIE, true)."]");
        if (empty($_COOKIE[$cookie_name])) {

            //$this->logger->log(LOG_INFO,"Cookie [".$cookie_name."] not found!!!");
            $cookie = Utils::generateRandom(32);
            /**
             * Cookies don't woth is path - not a root
             * @todo Try to understand why!
            */
            if ($ttl === false) {
                $ttl = $this->_ttl;
            }

            setcookie($cookie_name, $cookie, time() + $ttl, $path, '', false, true);
            $status = 'created';
        } else {
            $cookie = $_COOKIE[$cookie_name];
            $status = 'exists';
        }
        return $cookie;
    }
}